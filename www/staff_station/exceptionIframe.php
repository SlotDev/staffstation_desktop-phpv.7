<?php 
  if(!isset($_SESSION)){
      session_start();
  }
  $lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    $_SESSION['lang'] = $_GET['lang'];
      if($_SESSION['lang'] == "eng"){
        include "lang_eng.php";
      }
      else{
        include "lang_th.php";
      }
    }
    else if ($_SESSION['lang'] == 'eng') {
      include "lang_eng.php";
    }
    else{
      include "lang_th.php";
    }
  include "connect_db.php"; 
	
?>
<html lang="en">
 <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link href="jquery-form-validator/validator.css" rel="stylesheet">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
	
    <script src="jquery-form-validator/jquery.form.validator-th.min.js"></script>
    <script src="jquery-form-validator/security.js"></script>
    <script src="jquery-form-validator/file.js"></script>
 </head>

<body>
<div class="container-fluid">
	<form class="form-inline" method="post">
		<div class="form-group" align="center">
		    <label>รหัสหนังสือที่ได้รับการยกเว้น : </label>
		    <input name="book_id" placeholder="กรอกรหัสหนังสือ" class="form-control" data-validation="required" autofocus="autofocus">
		 </div>
		<input type="submit" name="submit_ex" class="btn btn-primary" >
	</form>
	<?php
		if(isset($_POST['submit_ex'])){
			$sql_ex = "SELECT re_ss_book_id,re_ss_book_name FROM report_staff_station WHERE re_ss_book_id = '".$_POST['book_id']."'";
			$query_ex = mysqli_query($conn,$sql_ex);
			$row_ex = mysqli_num_rows($query_ex);
			$res_ex = mysqli_fetch_array($query_ex,MYSQLI_BOTH);
			if($row_ex != 0){
				$sql_1 = "SELECT book_id,book_name FROM exception_list_book WHERE book_id = '".$_POST['book_id']."'";
			    $query_1 = mysqli_query($conn,$sql_1);
			    $row_1 = mysqli_num_rows($query_1);
			    if($row_1 == 0){
					$insert_ex = "INSERT INTO exception_list_book(book_id,book_name,update_date,update_user) VALUES ('".$_POST['book_id']."','".$res_ex['re_ss_book_name']."',now(),'$name')";
					$query_insert_ex = mysqli_query($conn,$insert_ex);
				}
				else{ echo "<font color=\"red\">** รหัสซ้า **</font>"; }
			}
			else { echo "<font color=\"red\">** รหัสนี้ไม่มีในระบบ **</font>"; }
		}
	?>
	<div class="panel panel-default">
	  <div class="panel-body" style="max-height: 80%;overflow-y: scroll;">
	    <table class="table">
	    	<thead>
	    		<th>ลำดับ</th>
	    		<th>รหัสหนังสือ</th>
	    		<th>ชื่อหนังสือ</th>
	    		<th></th>
	    	</thead>
	    	<tbody>
	    		<?php
	    			$i = 0;
			    	$sql = "SELECT book_id,book_name FROM exception_list_book ORDER BY book_id ASC";
			    	$query = mysqli_query($conn,$sql);
			    	while($res = mysqli_fetch_array($query,MYSQLI_BOTH)){
			    		$i++;
			    ?>
	    		<tr>
	    			<td><?php echo $i;?></td>
	    			<td><?php echo $res['book_id']; ?></td>
	    			<td><?php echo $res['book_name']; ?></td>
	    			<td><button type="button" class="btn btn-link" style="outline-style:none;"><img src="img/bin.png" style="width:30px" onmouseover="this.src='img/bin_1.png'" onmouseout="this.src='img/bin.png'"></button></td>
	    		</tr>
	    		<?php } ?>
	    	</tbody>
	    </table>
	  </div>
	</div>
</div>


</body>

</html>