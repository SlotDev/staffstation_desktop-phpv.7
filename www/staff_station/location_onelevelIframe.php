<?php
	if(!isset($_SESSION)){
      session_start();
  }
  $lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    $_SESSION['lang'] = $_GET['lang'];
      if($_SESSION['lang'] == "eng"){
        include "lang_eng.php";
      }
      else{
        include "lang_th.php";
      }
    }
    else if ($_SESSION['lang'] == 'eng') {
      include "lang_eng.php";
    }
    else{
      include "lang_th.php";
    }
  include "connect_db.php";
	$id = $_GET['id'];

  $sql_area = "SELECT parent_id,location_name FROM locations WHERE id = '$id'";
  $query_area = mysqli_query($conn,$sql_area);
  $res_area = mysqli_fetch_array($query_area,MYSQLI_BOTH);
  echo "<h4><strong> location : </strong>".$res_area['location_name']."</h4>";
?>
<html lang="en">
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="jquery-form-validator/jquery.min.js"></script>
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/hint.css">
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
  <link href="jquery-form-validator/validator.css" rel="stylesheet">
  <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
  <script src="jquery-form-validator/jquery.form.validator-th.min.js"></script>
  <script src="jquery-form-validator/security.js"></script>
  <script src="jquery-form-validator/file.js"></script>

</head>

<body>
<div class="container-fliud">
  <ul class="nav nav-tabs">
    <li role="presentation" <?php if($_GET['page'] == 'area_info'){ echo "class=\"active\""; } ?>><a href="location_onelevelIframe.php?id=<?php echo $id; ?>&page=area_info"><?php echo $lang_data_locate; ?></a></li>
    <li role="presentation" <?php if($_GET['page'] == 'book_info'){ echo "class=\"active\""; } ?>><a href="location_onelevelIframe.php?id=<?php echo $id; ?>&page=book_info"><?php echo $lang_book_info; ?> </a></li>
    <!--<li role="presentation" <?php if($_GET['page'] == 'book_add'){ echo "class=\"active\""; } ?>><a href="location_onelevelIframe.php?id=<?php echo $id; ?>&page=book_add"><?php echo $lang_book_add; ?> </a></li>-->
  </ul>
  <?php 
          switch ($_GET["page"]) {
          case "area_info":
            include("area_info.php");
            break;
          case "book_info":
            include("book_info.php");
            break;
          case "book_add":
            include("book_add.php");
            break;
          }
  ?>
</div>


 <script>
	 $.validate({
		 modules: 'security, file',
		 onModulesLoaded: function () {
		 	$('input[name="pass_confirmation"]').displayPasswordStrength();
		 }
	 });
 </script>
 </body>

 </html>