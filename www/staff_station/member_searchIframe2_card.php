<?php 
  if(!isset($_SESSION)){
      session_start();
  }
include "configuration.php";
include "connect_db.php";
  
?>
<html lang="en">
 <head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
 </head>
	<style type="text/css">
		.bgSize{
			background-size: 50px 50px;
			background-repeat: no-repeat;
			background-position: center;
		}
	</style>
<body>

<?php
	if($_GET['id'] != '' && $_GET['name'] != ''){
		$id = $_GET['id'];
		$name = "<br>".$_GET['name'];
		$borrowed = $_GET['borrowed'];
		$booked = $_GET['booked'];
		$overdue = $_GET['overdue'];
		$fine = $_GET['fine'];
		$amount = $_GET['amount'];
                $member_image = $_GET['$member_image'];
		
		
		$member = "SELECT access_card_uid,access_card_finger,access_card_patron_id,access_card_patron_name  FROM access_card WHERE access_card_patron_id = '$id'";
		$query_member = mysqli_query($conn,$member);
		$res_member = mysqli_fetch_array($query_member,MYSQLI_BOTH);

		/*
		$balance = "SELECT balance_total FROM balance WHERE balance_memberID = '$id'";
	    $query_balance = mysqli_query($conn,$balance);
	    $row_balance = mysqli_num_rows($query_balance);
		$res_balance = mysqli_fetch_array($query_balance,MYSQLI_BOTH);
		*/
?>
	<div class="container-fluid">	
		<div class="row">
			<div class="col-sm-4 col-lg-12" align="center">
				<!---->
                                <?php
                                    function is_webfile($webfile)
                                    {
                                     $fp = @fopen($webfile, "r");
                                     if ($fp !== false)
                                      fclose($fp);

                                     return($fp);
                                    }
                                    if(is_webfile($path_image_member . $member_image)){
                                        echo '<img src="' . $path_image_member . $member_image . '" class="img-circle" style="height:15%;" >'. "<br>";
                                    }else{
                                        echo '<img src="img/human.jpg" class="img-circle" style="height:15%;" >';
                                    }
                                ?>
			</div>
			<div class="col-sm-8 col-lg-12"  align="center">
				<p><?php echo $name; ?></p>
				<p>รหัสสมาชิก : <?php echo $id; ?></p>
				<p>รหัสการ์ด : <?php echo $res_member['access_card_uid']; ?></p>
				
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-12 col-lg-12" >
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td width="33.33%" align="center"><font size="1">Borrowed</font></td>
						<td width="33.33%" align="center"><font size="1">Booked</font></td>
						<td width="33.33%" align="center"><font size="1">Overdue</font></td>
					</tr>
					<tr>
						<td width="33.33%" height="60" align="center" class="bgSize" background="img/circle.png"><?php echo $borrowed; ?></td>
						<td width="33.33%" height="60" align="center" class="bgSize" background="img/circle.png"><?php echo $booked; ?></td>
						<td width="33.33%" height="60" align="center" class="bgSize" background="img/circle.png"><?php echo (!empty($overdue)? "<font color='red'>" . $overdue . "</font>" : "0"); ?></td>
					</tr>
				</table>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4" align="right">
				<label class="form-lable">ค่าปรับ</label> 
			</div>
			<div class="col-xs-6 form-group has-error">
				<input type="text" disabled="true" <?php echo (!empty($fine)? "class='form-control' style='color:red;'" : "class='form-control'"); ?> value="<?php echo (!empty($fine)? number_format($fine) : "0"); ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4" align="right">
				<label>เงินคงเหลือ</label>
			</div>
			<div class="col-xs-6 form-group has-success">
				<input type="text" value="<?php echo $amount; ?>" disabled="true" class="form-control">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12" align="center">
				<a href="#" onclick="top.clearInputUidEdit();window.parent.$('#modify').modal();"><img src="img/modify.png" style="width:15%" onmouseover="this.src='img/modify_1.png'"onmouseout="this.src='img/modify.png'"></a>
				<a href="#" onclick="window.parent.$('#deletemodal').modal();"><img src="img/bin.png" style="width:15%" onmouseover="this.src='img/bin_1.png'"onmouseout="this.src='img/bin.png'"></a>
			</div>
		</div>
	</div>
<?php } ?>


</body>

</html>