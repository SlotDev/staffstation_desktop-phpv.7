<?php 

	include "connect_db.php";
	include("api_hongkhai/nusoap.php");

	$client = new nusoap_client($path_api,true); 
	$write = $_POST['write_tag'];

	$lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    	$_SESSION['lang'] = $_GET['lang']; //เก็บค่าของภาษาไว้ใน SESSION
      	if($_SESSION['lang'] == "eng"){
        	include "lang_eng.php";
      	}
      	else{
        	include "lang_th.php";
      	}
    }
    else if ($_SESSION['lang'] == 'eng') {
      	include "lang_eng.php";
    }
    else{
      	include "lang_th.php";
    }
  	
?>
<html lang="en">
 <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link href="jquery-form-validator/validator.css" rel="stylesheet">

	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="jquery-form-validator/jquery.form.validator-th.min.js"></script>
	<script src="jquery-form-validator/security.js"></script>
	<script src="jquery-form-validator/file.js"></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
	<script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
 </head>

<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<span class="pull-left"><?php echo $lang_encode; ?></span>
			</div>
			<hr>
			<div class="col-md-1">&nbsp;</div>
			<div class="col-md-10 col-sm-12">
				<div class="panel panel-primary" style="width:100%;">
					<div class="panel-body">
						<div class="col-md-12 col-sm-12">&nbsp;</div>
						<?php
							$checkstatus = array( 'Barcode' => $write);
							$result = $client->call('checkstatus',$checkstatus); 
							foreach ($result as $data_array) {
								$barcode = $data_array["barcode"];
								if($data_array["error"] == 1){ echo $lang_not_found_barcode; }
								else{
									$sql_tag = "SELECT book_id FROM report_staff_station WHERE book_id = '$barcode'";
									$query_tag = mysqli_query($conn,$sql_tag) or die(mysqli_error($conn));
									$row_tag = mysqli_num_rows($query_tag);
									$res_tag = mysqli_fetch_array($query_tag,MYSQLI_BOTH);
									
								
						?>
						<div class="col-md-4 col-sm-4" align="center">
							<img src="<?php echo $data_array["image_book"]; ?>" style="width:150px">
						</div>
						<div class="col-md-8 col-sm-8">
							<table class="table">
								<tr>
									<th><?php echo $lang_barcode; ?></th>
									<td><?php echo $barcode; ?></td>
								</tr>
								<tr>
									<th><?php echo $lang_call_no; ?></th>
									<td><?php echo $data_array["call_no"]; ?></td>
								</tr>
								<tr>
									<th><?php echo $lang_book_name; ?></th>
									<td><?php echo $data_array["media_name"]; ?></td>
								</tr>
								<tr>
									<th><?php echo $lang_status; ?></th>
									<?php if($data_array["chk_checkout"] == 'false'){ ?>
										<td><p class="text-success"><strong><?php echo $lang_available; ?></strong></p></td>
									<?php } else{ ?>
										<td><p class="text-danger"><strong><?php echo $lang_unavailable; ?></strong></p></td>
									<?php }?>
								</tr>
								<tr>
									<th><?php echo $lang_security; ?></th>
									<?php 
										if($row_tag == 0){
									?>
										<td><p class="text-danger"><strong>Disabled</strong></p></td>
									<?php } else{ ?>
										<td><p class="text-success"><strong>Enabled</strong></p></td>
									<?php } ?>
								</tr>
							</table><br>
							<form class=" form-inline" onsubmit="return false;">
								<div class="form-group">
									<label><?php echo $lang_tag_id; ?></label>
									<input type="text" class="form-control" id="tag_encode" name="tag_encode" onkeyup="if(event.keyCode=='13'){top.validate_tag_onkeyup(document.getElementById('tag_encode').value,'<?php echo $barcode; ?>','<?php echo urlencode($data_array["media_name"]); ?>','<?php echo $data_array["call_no"]; ?>');}" autofocus="autofocus" data-validation="required" data-validation-error-msg="<?php echo $lang_tag_false; ?>" >
								</div>
								<button type="button" class="btn btn-success" onclick="top.validate_tag(document.getElementById('tag_encode').value,'<?php echo $barcode; ?>','<?php echo urlencode($data_array["media_name"]); ?>','<?php echo $data_array["call_no"]; ?>');"><?php echo $lang_create; ?></button>
							</form>
						</div>
						<?php }} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	$remove[] = "'";
	$remove[] = '"';
	//$remove[] = "-"; // just as another example
	$media_name = str_replace( $remove, "", $data_array["media_name"]);
	?>
 	 <script>
		 $.validate({
			 modules: 'security, file',
			 onModulesLoaded: function () {
			 	$('input[name="pass_confirmation"]').displayPasswordStrength();
			 }
		 });

		 function tag_encode_onkeyup(){

		 }

		 function tag_encode_onclick(){

		 }
	 </script>
</body>
</html>
