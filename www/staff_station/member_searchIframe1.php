<?php 
  if(!isset($_SESSION)){
      session_start();
  }
  include "configuration.php";
  include "connect_db.php";
  include("api_hongkhai/nusoap.php");

  
  $type = $_GET['type'];
  $keyword = $_GET['keyword'];
  if($type == 'name'){
  	$sql_search = "SELECT access_card_uid,access_card_finger,access_register_date,access_card_expire_date,access_card_patron_id,access_card_patron_name 
  				FROM access_card 
  				WHERE access_card_patron_name LIKE '%".trim($keyword)."%' ORDER BY access_card_patron_id ASC";
  }
  else if ($type == 'id'){
  	$sql_search = "SELECT access_card_uid,access_card_finger,access_register_date,access_card_expire_date,access_card_patron_id,access_card_patron_name 
  				FROM access_card 
  				WHERE access_card_patron_id LIKE '%".trim($keyword)."%' ORDER BY access_card_patron_id ASC";
  }else{
	 $sql_search = "SELECT access_card_uid,access_card_finger,access_register_date,access_card_expire_date,access_card_patron_id,access_card_patron_name 
  				FROM access_card WHERE access_card_patron_id ='No data'";
  }
  echo $sql_search;
  $query_search = mysqli_query($conn,$sql_search);
?>
<html lang="en">
 <head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
	<style type="text/css">
		.bgSize{
			background-size: 50px 50px;
			background-repeat: no-repeat;
			background-position: center;
		}
	</style>

 </head>
<body>
<div class="container-fluid">
        <div class="row">&nbsp;</div>
	<div class="list-group">
		<?php 
            function is_webfile($webfile)
            {
                $fp = @fopen($webfile, "r");
				if ($fp !== false)
                    fclose($fp);
                    return($fp);
            }
                
			//$client = new nusoap_client($path_api,true); 
			$i = 0;
			
			while($res_search = mysqli_fetch_array($query_search)){
			$i++;
            $name = $res_search['access_card_patron_name'];
			$id = $res_search['access_card_patron_id'];
			$finger_id = $res_search['access_card_finger'];
			$uid = $res_search['access_card_uid'];
			$access_register_date = $res_search['access_register_date'];
			$access_card_expire_date = $res_search['access_card_expire_date'];
			$register = date_format(date_create($access_register_date),"d/m/Y");
			$expire = date_format(date_create($access_card_expire_date),"d/m/Y");

		?>
              
			<a href="#" onclick="parent.member_data('<?php echo $name; ?>','<?php echo $id;?>','<?php echo $uid;?>','<?php echo $finger_id;?>','-','<?php echo "0";?>','-','-','-','-');  return false;" class="list-group-item">
				<div class="row">
					<div class="col-xs-2 col-sm-1"  align="center">
						<br><?php echo $i; ?>
					</div>
					<div class="col-xs-10 col-sm-4">
						<div class="row">
							<!--
							<div class="col-xs-4 col-sm-4">
							<?php
                                //echo $path_image_member . $id . ".jpg";
								/*
                                if(is_webfile($path_image_member . $id . ".jpg")){
                                    echo '<img src="' . $path_image_member . $id . '.jpg" class="img-circle" style="height:15%;" >'. "<br>";
                                }else{
                                    echo '<img src="img/human.jpg" class="img-circle" style="height:15%;" >';
                                }
								*/
								echo '<img src="' . $member_image . '" class="img-circle" style="height:15%;" >'. "<br>";
                            ?>
							</div>
							-->
							<div class="col-xs-11 col-sm-11">
							
                                <?php
                                    echo "<p>ชื่อ : " . $name . "</p>";
                                    echo "<p>รหัสสมาชิก : ". $id ."</p>";
                                ?>
                                                                
							</div>
						</div>
					</div>
					<div class="col-xs-12 visible-xs"><hr></div>
					<div class="col-xs-7 col-sm-4" align="left">
						<p>รหัสบัตร : <?php echo (!empty($uid)? $uid : "-");?></p>
						<p>รหัสลายนิ้วมือ : <?php echo (!empty($finger_id)? "Yes" : "No");?></p>
					</div>
					<div class="col-xs-5 col-sm-3">
						<!--<p>วันที่สมัครสมาชิก <?php echo $register;?></p>
						<p>หมดอายุวันที่ <?php echo $expire;?></p>-->
					</div>
				</div>
			</a>
		<?php } ?>
	</div>
	ค้นหาพบ <?php echo $i; ?> รายการ
</div>
</body>

</html>