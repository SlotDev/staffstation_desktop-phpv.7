<?php
	if(!isset($_SESSION)){
   	session_start();
  	}
    $lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
      $_SESSION['lang'] = $_GET['lang']; //เก็บค่าของภาษาไว้ใน SESSION
        if($_SESSION['lang'] == "eng"){
          include "lang_eng.php";
        }
        else{
          include "lang_th.php";
        }
    }
    else if ($_SESSION['lang'] == 'eng') {
      include "lang_eng.php";
    }
    else{
      include "lang_th.php";
    }
  include "connect_db.php";

function sub_menu($parent_id = 0){
  global $conn;
  $sql1 = "SELECT id,parent_id,location_name FROM locations WHERE parent_id = '$parent_id' ORDER BY location_name asc";
  $query1 = mysqli_query($conn,$sql1);
  $row1 = mysqli_num_rows($query1);
    if($row1 > 0){
      if($parent_id != 0){ echo "<ul>"; }
      while($res1 = mysqli_fetch_array($query1,MYSQLI_BOTH)){
        $id=$res1['id'];
        echo "<li><a href=\"#\" onclick=\"parent.select_level('".$id."');\"> ".$res1['location_name']."</a>";
        sub_menu($res1['id']);
        echo "</li>";
      }
      if($parent_id != 0){ echo "</ul>"; }
    }
}
?>
<html lang="en">
 <head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/jquery.navgoco.css" >
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
  <script src="bootstrap-3.3.5-dist/js/jquery.navgoco.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
 </head>
 <style type="text/css">
  .nav a:hover, .nav li.active > a {
    color: #040404;
    background-color: #B9CCD0;
}
 </style>
<body>
<div class="container-fluid">

<?php
    $sql1 = "SELECT location_name FROM locations WHERE parent_id = '0'";
    $query1 = mysqli_query($conn,$sql1);
?>
<ul id="demo1" class="nav">
    <?php sub_menu(); ?>     
</ul>
<p class="external">
  <a href="#" id="collapseAll"><?php echo $lang_collapse; ?></a> | <a href="#" id="expandAll"><?php echo $lang_expand; ?></a>
</p>

<script type="text/javascript">
     $(document).ready(function() {
  // Initialize navgoco with default options
  $("#demo1").navgoco({
    caretHtml: '',
    accordion: false,
    openClass: 'open',
    save: true,
    cookie: {
      name: 'navgoco',
      expires: false,
      path: '/'
    },
    slide: {
      duration: 400,
      easing: 'swing'
    },
    // Add Active class to clicked menu item
    onClickAfter: function(e, submenu) {
      e.preventDefault();
      $('#demo1').find('li').removeClass('active');
      var li =  $(this).parent();
      var lis = li.parents('li');
      li.addClass('active');
      lis.addClass('active');
    },
  });

  $("#collapseAll").click(function(e) {
    e.preventDefault();
    $("#demo1").navgoco('toggle', false);
  });

  $("#expandAll").click(function(e) {
    e.preventDefault();
    $("#demo1").navgoco('toggle', true);
  });
});
</script>
</div>

</body>

</html>