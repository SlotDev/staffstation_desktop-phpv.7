<?php 
  if(!isset($_SESSION)){
      session_start();
  }
include "configuration.php";
include "connect_db.php";
include("api_hongkhai/nusoap.php");
?>
<html lang="en">
 <head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
 </head>
	<style type="text/css">
		.bgSize{
			background-size: 50px 50px;
			background-repeat: no-repeat;
			background-position: center;
		}
	</style>
<body>

<?php
	if($_GET['id'] != '' && $_GET['name'] != ''){
		$id = $_GET['id'];
		$name = "<br>".$_GET['name'];
		$borrowed = $_GET['borrowed'];
		$booked = $_GET['booked'];
		$overdue = $_GET['overdue'];
		$fine = $_GET['fine'];
		$amount = $_GET['amount'];
        $member_image = $_GET['$member_image'];
		
		
		$member = "SELECT access_card_uid,access_card_finger,access_card_patron_id,access_card_patron_name  FROM access_card WHERE access_card_patron_id = '$id'";
		$query_member = mysqli_query($conn,$member);
		$res_member = mysqli_fetch_array($query_member,MYSQLI_BOTH);
		
		$card_uid = $res_member['access_card_uid'];
		$finger_id = $res_member['access_card_finger'];

		$client = new nusoap_client($path_api,true); 
		$checkpatron = array(
			'PatronID' => $id
		);
		$result = $client->call('checkpatron',$checkpatron);
			
			
		$result2 = $result['0'];
		$Patron_Error = $result2["error"];
		$Patron_Error_description = $result2["error_description"];
		$Patron_chk_member = $result2["chk_member"];
        //$Patron_id = $result2["member_cardID"];
		$Patron_id = $id;
		$Patron_Name = $result2["name"];
		$Patron_maxcheckout = $result2["maxcheckout"];
		$Patron_fine = $result2["fine"];
		$Patron_fine_count = sizeof($result2["fine_list"]);
		$Patron_fine_list = $result2["fine_list"];
		$checkout_list =$result2['checkout_list'];
		$Patron_chk_permit = $result2["chk_permit"];
		$Patron_checkout_list = $result2["checkout_list"];
        $checkout_list_count = sizeof($result2["checkout_list"]);
		$Patron_num_overdue = $result2["num_overdue"];
		$Patron_num_checkedout = $result2["num_checkedout"];
		//$member_image = "http://172.16.106.70/showImage.php?type=member&barcode=".$id;//$result2["image_profile"];
		
        if(empty(trim($Patron_fine))){
             $Patron_fine = "0";  
        }		
?>
	<div class="container-fluid">	
		<div class="row">
			<div class="col-sm-4 col-lg-12" align="center">
				<!---->
                                <?php
                                    function is_webfile($webfile)
                                    {
                                     $fp = @fopen($webfile, "r");
                                     if ($fp !== false)
                                      fclose($fp);

                                     return($fp);
                                    }
									/*
                                    if(is_webfile($path_image_member . $member_image)){
                                        echo '<img src="' . $path_image_member . $member_image . '" class="img-circle" style="height:15%;" >'. "<br>";
                                    }else{
                                        echo '<img src="img/human.jpg" class="img-circle" style="height:15%;" >sssssssssssss';
                                    }
									*/
									if(is_webfile($path_image_member . $id . ".jpg")){
                                        echo '<img src="' . $path_image_member . $id . '.jpg" class="img-circle" style="height:15%;" >'. "<br>";
                                    }else{
                                        echo '<img src="img/human.jpg" class="img-circle" style="height:15%;" >';
                                    }
									
									//echo '<img src="' . $member_image . '" class="img-circle" style="height:15%;" >'. "<br>";
                                ?>
			</div>
			<div class="col-sm-8 col-lg-12"  align="center">
				<p><?php echo $name; ?></p>
				<p>รหัสสมาชิก : <?php echo $id; ?></p>
				<p>รหัสการ์ด : <?php echo $card_uid;?></p>
				<?php
					if(!$finger_id == ""){
                        echo "<p>ลายนิ้วมือ : <font color='green'>ลงทะเบียนแล้ว</font></p>";
                    }else{
                        echo "<p>ลายนิ้วมือ : <font color='orange'>ยังไม่ลงทะเบียน</font></p>";
                    }
				
				
					if(((boolean)$Patron_chk_member) && (!empty(trim($Patron_id)))){
                        echo "<p>การเป็นสมาชิกห้องสมุด : <font color='green'>ใช่</font></p>";
                    }else{
                        echo "<p>การเป็นสมาชิกห้องสมุด :  : <font color='orange'>ไม่ใช่</font></p>";
                    }
				
				?>
				
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-12 col-lg-12" >
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td width="33.33%" align="center"><font size="1">หนังสือที่ยืม</font></td>
						<td width="33.33%" align="center"><font size="1">หนังสือจอง</font></td>
						<td width="33.33%" align="center"><font size="1">หนังสือเกินกำหนดส่ง</font></td>
					</tr>
					<tr>
						<td width="33.33%" height="60" align="center" class="bgSize" background="img/circle.png"><?php echo $checkout_list_count; ?></td>
						<td width="33.33%" height="60" align="center" class="bgSize" background="img/circle.png"><?php echo "0"; ?></td>
						<td width="33.33%" height="60" align="center" class="bgSize" background="img/circle.png"><?php echo (!empty($Patron_num_overdue)? "<font color='red'>" . $Patron_num_overdue . "</font>" : "0"); ?></td>
					</tr>
				</table>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4" align="right">
				<label class="form-lable">ค่าปรับ</label> 
			</div>
			<div class="col-xs-6 form-group has-error">
				<input type="text" disabled="true" <?php echo (!empty($Patron_fine)? "class='form-control' style='color:red;'" : "class='form-control'"); ?> value="<?php echo (!empty($Patron_fine)? number_format($Patron_fine) : "0"); ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12" align="center">
				<a href="#" onclick="top.clearInputUidEdit();window.parent.$('#modify').modal();"><img src="img/modify.png" style="width:15%" onmouseover="this.src='img/modify_1.png'"onmouseout="this.src='img/modify.png'"></a>
				<a href="#" onclick="window.parent.$('#deletemodal').modal();"><img src="img/bin.png" style="width:15%" onmouseover="this.src='img/bin_1.png'"onmouseout="this.src='img/bin.png'"></a>
			</div>
		</div>
	</div>
<?php } ?>
</body>
</html>