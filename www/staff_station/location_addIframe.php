<?php 
  if(!isset($_SESSION)){
      session_start();
  }
  $lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
      $_SESSION['lang'] = $_GET['lang']; //เก็บค่าของภาษาไว้ใน SESSION
        if($_SESSION['lang'] == "eng"){
          include "lang_eng.php";
        }
        else{
          include "lang_th.php";
        }
    }
    else if ($_SESSION['lang'] == 'eng') {
      include "lang_eng.php";
    }
    else{
      include "lang_th.php";
    }
  include "connect_db.php";
?>
<html lang="en">
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

  
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/hint.css">
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
  <link href="jquery-form-validator/validator.css" rel="stylesheet">
  <script src="jquery-form-validator/jquery.min.js"></script>
  <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
  <script src="jquery-form-validator/jquery.form.validator-th.min.js"></script>
  <script src="jquery-form-validator/security.js"></script>
  <script src="jquery-form-validator/file.js"></script>

</head>

<body>
<div class="container-fliud">
<h4><strong> <?php echo $lang_location_main; ?></strong> </h4>
<!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#add_location" aria-controls="add_location" role="tab" data-toggle="tab"><?php echo $lang_add_locate; ?></a></li>
    <li role="presentation"><a href="#info_location" aria-controls="info_location" role="tab" data-toggle="tab"><?php echo $lang_data_locate; ?> </a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="add_location"><br>
		<form align="center" class="form-inline" method="post" target="iframe_add" action="location_addIframe.php">
		  <div class="form-group">
		  	<label><?php echo $lang_addby; ?> </label>
		    <select class="form-control" name="type">
			  <option value="name"><?php echo $lang_name; ?> </option>
			</select>
		  </div>
		  <div class="form-group">
		    <input type="text" name="add_name" data-validation="required" data-validation-error-msg="<?php echo $lang_validate; ?>"class="form-control">
		  </div>
		  <button type="submit" name="add" class="btn btn-default"><?php echo $lang_add; ?> </button>				
		</form>
	
	<?php
	  if(isset($_POST['add_name'])){
	  	$_SESSION['ss_name'][$_POST['add_name']] = $_POST['add_name'];
	  
	?>
	<div class="container">
		<form method="post" name="form1">
		<table class="table table-hover table-condensed" style="width:100%;">
			<thead >
				<tr>
					<th><?php echo $lang_name; ?> </th>
					<th></th>
					<th><?php echo $lang_delete; ?> </th>
				</tr>
			</thead>
			<tbody>
			<?php   
				 if(count($_SESSION['ss_name'])>0){  
				 foreach($_SESSION['ss_name'] as $key=>$value){ 
			?> 
				<tr>
					<td><?php echo $_SESSION['ss_name'][$key]; ?></td>
					<td><?php echo $value; ?></td>
					<td><a href="location_addIframe.php?del=<?php echo $key; ?>"><span class="glyphicon glyphicon-remove"></span></a></td>
				</tr>
			<?php }} ?>
			</tbody>
		</table>
			<div align="right">
				<button type="submit" name="submit_add_location" onclick="parent.add();" class="btn btn-link"><img src="img/correct.png" onmouseover="this.src='img/correct_1.png'"onmouseout="this.src='img/correct.png'" style="width:65%;"></button>
			</div>
		</form>
	</div>
	<?php }?></div>
    <div role="tabpanel" class="tab-pane" id="info_location">
    	<table class="table table-hover table-condensed" style="width:100%;">
			<thead>
				<tr>
					<th width="10%"><?php echo $lang_no; ?> </th>
					<th width="40%"><?php echo $lang_location_name; ?> </th>
					<th width="10%"><?php echo $lang_edit; ?> </th>
					<th width="10%"><?php echo $lang_move; ?> </th>
					<th width="10%"><?php echo $lang_delete; ?> </th>
				</tr>
			</thead>
			<tbody>
				<?php
					$i_area = 0;
					$sql_area1 = "SELECT id,location_name FROM locations WHERE parent_id = '0'";
					$query_area1 = mysqli_query($conn,$sql_area1);
					while($res_area1 = mysqli_fetch_array($query_area1,MYSQLI_BOTH)){
						$i_area++;
						$position_id = $res_area1['id'];
						$sql_book1 = "SELECT position_id FROM inventory WHERE position_id = '$position_id'";
						$query_book1 = mysqli_query($conn,$sql_book1);
						$row_book1 = mysqli_num_rows($query_book1);
						$sql_book2 = "SELECT parent_id FROM locations WHERE parent_id = '$position_id'";
						$query_book2 = mysqli_query($conn,$sql_book2);
						$row_book2 = mysqli_num_rows($query_book2);
				?>
					<tr>
						<form method="post" name="form2">
							<td><?php echo $i_area; ?></td>
								<input type="hidden" name="id_lo" value="<?php echo $res_area1['id']; ?>">
							<td><?php echo $res_area1['location_name']; ?></td>
							<td><button type="button" class="btn btn-link" style="outline-style:none;"  onclick="parent.modify1('<?php echo $res_area1['id']; ?>','<?php echo $res_area1['location_name']; ?>'); return false;"><img src="img/modify.png" style="width:30px" onmouseover="this.src='img/modify_1.png'" onmouseout="this.src='img/modify.png'"></button></td>
							<td><button type="button" class="btn btn-link" style="outline-style:none;" onclick="parent.move1('<?php echo $res_area1['id']; ?>','<?php echo $res_area1['location_name']; ?>'); return false;"><img src="img/return_button.png" style="width:30px" onmouseover="this.src='img/return_button_1.png'" onmouseout="this.src='img/return_button.png'"></button></td>
							<?php if($row_book2 != 0){ ?>
								<td><button type="button" class="btn btn-link" style="outline-style:none;"  OnClick="alert('มี location ย่อย ไม่สามารถลบได้');return false;"><img src="img/bin.png" style="width:30px" onmouseover="this.src='img/bin_1.png'" onmouseout="this.src='img/bin.png'"></button></td>
							<?php }else if($row_book1 == 0){ ?>
								<td><button type="submit" name="delete_lo" class="btn btn-link" style="outline-style:none;"  OnClick="return confirm('ยืนยันการลบ location');"><img src="img/bin.png" style="width:30px" onmouseover="this.src='img/bin_1.png'" onmouseout="this.src='img/bin.png'"></button></td>
							<?php }else{ ?>
								<td><button type="button" class="btn btn-link" style="outline-style:none;"  OnClick="alert('มีหนังสือใน location ไม่สามารถลบได้');return false;"><img src="img/bin.png" style="width:30px" onmouseover="this.src='img/bin_1.png'" onmouseout="this.src='img/bin.png'"></button></td>
							<?php } ?>
						</form>
					</tr>
				<?php
					}//while
				?>
			</tbody>
		</table>
    </div>
  </div>


 <script>
	 $.validate({
		 modules: 'security, file',
		 onModulesLoaded: function () {
		 	$('input[name="pass_confirmation"]').displayPasswordStrength();
		 }
	 });
 </script>
 </body>

 </html>
<?php
	if(isset($_GET['del'])){
		$del = $_GET['del'];
		unset($_SESSION['ss_name'][$del]);
		echo "<META http-equiv=\"REFRESH\" content=\"0;url=location_addIframe.php\">";
	}
	if(isset($_POST['delete_lo'])){
		$id_lo = $_POST['id_lo'];
		$del_lo = "DELETE FROM locations WHERE id = '$id_lo'";
		$query_del = mysqli_query($conn,$del_lo);
		echo '<script type="text/javascript">top.window.location.href = "head_location.php?page=location_creator";</script>';
	}

	if(isset($_POST['submit_add_location'])){
		if(count($_SESSION['ss_name'])>0){  
			foreach($_SESSION['ss_name'] as $key=>$value){ 
				echo $sql_add_lo = "INSERT INTO locations(location_name,parent_id) VALUES('$value','0')";
				$query_add_lo = mysqli_query($conn,$sql_add_lo);
				echo '<script type="text/javascript">top.window.location.href = "head_location.php?page=location_creator";</script>';

			}
		}
		unset($_SESSION['ss_name']);
		echo "<META http-equiv=\"REFRESH\" content=\"0;\">";
	}
?>