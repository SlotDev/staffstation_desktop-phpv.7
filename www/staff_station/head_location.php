<?php 
  if(!isset($_SESSION)){
      session_start();
  }
  $lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    $_SESSION['lang'] = $_GET['lang'];
      if($_SESSION['lang'] == "eng"){
        include "lang_eng.php";
      }
      else{
        include "lang_th.php";
      }
    }
    else if ($_SESSION['lang'] == 'eng') {
      include "lang_eng.php";
    }
    else{
      include "lang_th.php";
    }
  include "connect_db.php";
?>
<html lang="en">
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link href="jquery-form-validator/validator.css" rel="stylesheet">

	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="jquery-form-validator/jquery.form.validator-th.min.js"></script>
	<script src="jquery-form-validator/security.js"></script>
	<script src="jquery-form-validator/file.js"></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>

</head>

<body>
<div class="container-fliud">
     <div class="panel panel-primary" style="height:11%;background:#2e7ed0;" >
      <div class="panel-body">
        <div class="row">
        	<div class="col-sm-4 col-lg-4" align="left">
        		<img src="img/logo.png" width="50px">
        	</div>
        	<div class="col-sm-4 col-lg-4" align="center">
        		<p class="text-uppercase">
              <?php if(strlen($orga) < 15){ ?>
                <font color="#ffffff" size="6"><?php echo $orga; ?></font>
              <?php }else{ ?>
                <font color="#ffffff" size="5"><?php echo $orga; ?></font>
              <?php } ?>
            </p>
        	</div>
        	<div class="col-sm-4 col-lg-4" align="right">
            <a href="head.php?page=main_menu"><img src="img/home.png" width="30px" onmouseover="this.src='img/home_1.png'"onmouseout="this.src='img/home.png'"></a>
              &nbsp;
            <a href="?page=<?php echo $_GET['page']; ?>&lang=th"><img src="img/flag_th.png" width="30px"></a>
            <a href="?page=<?php echo $_GET['page']; ?>&lang=eng"><img src="img/flag_en.png" width="30px"></a>
            <p><font color="#ffffff" size="2"><strong><?php echo $name; ?>&nbsp;  </strong></font><a href="logout.php"><strong><u><font color="#ffffff" size="1">  <?php echo $lang_logout; ?></font></u></strong></a></p>
          </div>
        </div>
      </div>
    </div>
   	<div class="panel panel-primary" style="height:83%;">
	  	<div class="panel-body">
	  		<div class="row">
	  			<div class="col-sm-12 col-lg-12">
					<?php 
					  switch ($_GET["page"]) {
					  case "location_menu":
					    include("location_menu.php");
					    break;
					  case "location_creator":
					    include("location_creator.php");
					    break;
					  case "location_designator":
					    include("location_designator.php");
					    break;
					  case "location_map":
					    include("location_map.php");
					    break;
					  default:
					    include("location_menu.php");
					  }
					?>
				</div>
		    </div>
		</div>
	</div>
</div>
</body>
 <script>
	 $.validate({
		 modules: 'security, file',
		 onModulesLoaded: function () {
		 	$('input[name="pass_confirmation"]').displayPasswordStrength();
		 }
	 });

	 
 </script>

 </html>