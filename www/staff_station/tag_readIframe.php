﻿<?php 
	include "connect_db.php";
	include("api_hongkhai/nusoap.php");

	$client = new nusoap_client($path_api,true); 

	$lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    	$_SESSION['lang'] = $_GET['lang']; //เก็บค่าของภาษาไว้ใน SESSION
      	if($_SESSION['lang'] == "eng"){
        	include "lang_eng.php";
      	}
      	else{
        	include "lang_th.php";
      	}
    }
    else if ($_SESSION['lang'] == 'eng') {
      	include "lang_eng.php";
    }
    else{
      	include "lang_th.php";
    } 
    //echo $_GET['out'];

?>
<html lang="en">
 <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
 </head>

<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<span class="pull-left"><?php echo $lang_read; ?></span>
		</div>
		<hr>
		<div id="addTagBorrow"></div>
		<div class="col-md-12 col-sm-12" align="center">
			<div class="panel panel-primary" style="width:90%;" align="left">
				<div class="panel-body" style="padding:1px;">
					<div class="list-group" style="width:100%;height:80%;overflow-x:hidden;">
						<?php

							$sum_tag = 0;
							$out_tag = explode('|', $_GET['out']);
							foreach ($out_tag as $value2) {
								if(strlen($value2) >= $validation_length1){
									$sum_tag++;
									$checkstatus = array( 'Barcode' => $value2);
									$result = $client->call('checkstatus',$checkstatus); 

									foreach ($result as $data_array) {
										//if($data_array["error"] == 1){ echo $data_array["error_description"]; }
										//else{
										/*
											$barcode = $data_array["barcode"];
											$sql_tag = "SELECT book_id FROM report_staff_station WHERE book_id = '$barcode'";
											$query_tag = mysqli_query($conn,$sql_tag);
											$row_tag = mysqli_num_rows($query_tag);
											$res_tag = mysqli_fetch_array($query_tag,MYSQLI_BOTH);
										*/
											$row_tag = 1;
											$res_tag = $value2;
											$re_ss_book_id = $value2;

										
						?>
							<?php if($row_tag != 0){ ?>
							<a href="#" onclick="parent.abc('<?php echo $res_tag ?>','<?php echo $path_image.$data_array["image_book"]; ?>','<?php echo $data_array["barcode"] ?>','<?php echo $data_array["call_no"] ?>','<?php echo urlencode($data_array["media_name"]); ?>','<?php echo $data_array["chk_checkout"]; ?>'); return false;"  class="list-group-item">
								<div class="row">
									<div class="col-md-2 col-sm-2 col-xs-4" align="center">
										<img src="<?php echo $path_image.$data_array['image_book']; ?>" style="width:60px;height:90px">
									</div>
									<div class="col-md-7 col-sm-7 col-xs-6">
										<?php if($row_tag == 1){ ?>
										<p class="lead" style="margin-bottom:5px;"><strong><?php echo $data_array["media_name"]; ?></strong></p>
										<p><strong><?php echo $lang_tag_id; ?> :</strong> <?php echo $re_ss_book_id ?></p>
										<?php } else{ ?>
										<p><strong><?php echo $lang_tag_id; ?> :</strong> N/A</p>
										<?php } ?>
									</div>
									<?php if($data_array["chk_checkout"] == 'false'){ ?>
										<div class="col-md-3 col-sm-3  col-xs-2"><p class="text-success"><strong><?php echo $lang_available; ?></strong></p></div>
									<?php } else{ ?>
										<div class="col-md-3 col-sm-3 col-xs-2"><p class="text-danger"><strong><?php echo $lang_unavailable; ?></strong></p></div>
									<?php } ?>
								</div>
							</a>
							<?php } else{ ?>
								<a href="#" class="list-group-item">
									<div class="row">
										<div class="col-md-2 col-sm-2 col-xs-4" align="center">
											<img src="<?php echo $path_image.$data_array['re_image']; ?>" style="width:60px;height:90px">
										</div>
										<div class="col-md-7 col-sm-7 col-xs-6">
											<p><strong><?php echo $lang_tag_id; ?> :</strong> <?php echo $data_array["barcode"] ?></p>
											<p><strong><?php echo $lang_not_found_book; ?></strong></p>
											
										</div>
										<div class="col-md-3 col-sm-3 col-xs-2"><p class="text-danger"><strong><?php echo $lang_tag_false; ?></strong></p></div>
									</div>
								</a>
								
							<?php } ?>
								
						<?php
										//}//else
									}//foreach
								}//if
							}//foreach
							if($sum_tag == 0){
								echo "<div align='center'><h2>".$lang_not_found_tag."</h2></div>";
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

</html>
