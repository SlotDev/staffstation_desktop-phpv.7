<div class="row">
	<div class="col-sm-4 col-lg-3">
		<div class="panel panel-primary" style="height:95%;">
			<div class="panel-body">
				<div class="col-lg-6"><span class="pull-left"><h4 class="text-info">สมาชิก</h4></span></div>
				<div class="col-lg-6" style="text-align: right;"><!--<span class="glyphicon glyphicon-print" aria-hidden="true"></span>&nbsp;&nbsp;<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>--></div>
				<iframe id="iframe_member" name="iframe_member" src="member_memberiframe.php" style="width:100%;height:90%;border:0px;"></iframe>
			</div>
		</div>
	</div>
	
	<div class="col-sm-8 col-lg-9">
  		<div class="panel panel-primary" style="height:95%;">
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12 col-lg-12">
						<h4 class="text-info">ค้นหาสมาชิก</h4>
					</div>
					<hr>
					<div class="col-sm-12 col-lg-12" align="center">
						<form class="form-inline" method="get" target="iframe_search" action="member_searchIframe1.php">
						  <div class="form-group">
						  	<label>ค้นหาโดย </label>
						    <select class="form-control" name="type">
                                                        <option value="id">รหัสสมาชิก</option>
							<option value="name">ชื่อสมาชิก</option>
                                                    </select>
						  </div>
						  <div class="form-group">
						    <input type="text" name="keyword" class="form-control" required="true">
						  </div>
						  <button type="submit" name="search" class="btn btn-default">ค้นหา</button>
						  <!--<a href="#" data-toggle="tooltip" data-placement="right" title="RFID ... D"><img src="img/signal.png" style="width:30px" onmouseover="this.src='img/signal_1.png'"onmouseout="this.src='img/signal.png'"></a>-->
						
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12" align="center">
						<div class="panel panel-info" style="height:65%;">
							<iframe id="iframe_search" name="iframe_search" src="member_searchmainIframe.php" style="width:95%;height:95%;border:0px;"></iframe>
						</div>
					</div>
                                        <!--
					<div class="col-sm-12 col-lg-12">
						<center><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>&nbsp;Page <input type="text" max="100" name="pagenumber" placeholder="0"  style=" width:50px;" > of 0<span class="glyphicon glyphicon-triangle-r" aria-hidden="true"></span> Total member 0</center>
					</div>
                                        -->
					<div class="col-sm-1 col-lg-1">&nbsp;</div>
					<div class="col-sm-5 col-lg-5">
						<a href="head_member.php?page=member_add"><img src="img/add_member.png" style="width:10%" onmouseover="this.src='img/add_member_1.png'"onmouseout="this.src='img/add_member.png'"></a>
					</div>
					<div class="col-sm-1 col-lg-1">&nbsp;</div>
				</div>
	    	</div>
	    </div>
	</div>
</div>

	<!-- Modal modify-->
	
	<div class="modal fade" id="modify" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">แก้ไขข้อมูลสมาชิก</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="well">
	      		<div class="row">
	        		<div class="col-sm-4 col-lg-4" align="right">
	        			<img src="img/human.jpg" class="img-circle" style="width:80px;height:80px;" >
	        		</div>
	        		<div class="col-sm-8 col-lg-8">
                                    <p id="idMemEdit"></p>
		  		    <p id="idMemName"></p>
		  		    <p id="idMemUID"></p>	
					<!--<p id="idMemFinger"></p>-->
	        		</div>
	        	</div>
	      	</div>
	      	<div class="panel panel-primary">
	  			<div class="panel-body">
				    <div id="divEditMember" align="center">
				    	<form id="form" class="form-inline">
				    		<div class="form-group">
							    <label>รหัสบัตรใหม่</label>
							    <input id="uidMsg" name="uidMsg" type="text" class="form-control" >
							</div>
							<br><br>
							<div class="form-group">
							    <label>รหัสลายนิ้วมือ</label>
								<textarea class="form-control" id="fingerMsg" name="fingerMsg" rows="8" style="width:500px;"></textarea>
							</div>
							<!--<a href="#" data-toggle="tooltip" data-placement="right" title="RFID ... D"><img src="img/signal.png" style="width:30px"></a>-->
							<input id="mem_id" name="mem_id" type="hidden" class="form-control" >
							<input name="action_type" type="hidden" class="form-control" value="edit">
							<!--<input name="action_type" type="hidden" class="form-control" value="delete">
							<input id="editMemberProcess" type="button" value="Edit"/>-->	
				    	<hr>
				    	<p class="lead">ต้องการเปลี่ยนแปลงข้อมูล</p>
				      	<a href="#" id="editMemberProcess" ><img src="img/correct.png" style="width:10%;" onmouseover="this.src='img/correct_1.png'"onmouseout="this.src='img/correct.png'"></a>
						<a href="#" data-dismiss="modal"><img src="img/incorrect.png" style="width:10%;" onmouseover="this.src='img/incorrect_1.png'"onmouseout="this.src='img/incorrect.png'"></a>
						</form>
					</div>
					<div id="divEditMemberStatusSuccess" style="display:none;" align="center">
						Success
						<br>
						<input type="button" onclick="divCloseMemEdit()" value="Close">
					</div>
					<div id="divEditMemberStatusFail" style="display:none;" align="center">
						Fail
						<br>
						<input type="button" onclick="divCloseMemEdit()" value="Close">
					</div>
					
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal delete-->
	<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">ลบสมาชิก</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="well" id="memDetailDelete">
	      		<div class="row" >
	        		<div class="col-sm-4 col-lg-4" align="right">
	        			<img src="img/human.jpg" class="img-circle" style="width:80px;height:80px;" >
	        		</div>
	        		<div class="col-sm-8 col-lg-8">
	        			<p id="idMemDelete"></p>
		  			<p id="idMemDeleteName"></p>
		  			<p id="idMemDeleteUID"></p>
	        		</div>
	        	</div>
	      	</div>
	      	<div class="panel panel-primary">
	  			<div class="panel-body">
				    <div id="deletemodalControl" align="center">
				    	<p class="lead">ต้องการลบข้อมูลสมาชิก</p>
				      	<a href="#" id="deleteMemberProcess"><img src="img/correct.png" style="width:10%;"></a>
					<a href="#" data-dismiss="modal"><img src="img/incorrect.png" style="width:10%;"></a>
				    </div>
					<div id="divDeleteMemberStatusSuccess" class="text-success" style="display:none;" align="center">
						Success
						<br>
						<input type="button" onclick="divCloseMemDelete()" value="Close">
					</div>
					<div id="divDeleteMemberStatusFail" class="text-danger" style="display:none;" align="center">
						Fail
						<br>
						<input type="button" onclick="divCloseMemDelete()" value="Close">
					</div>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>
<script>
$(document).ready(function(){
	$("#editMemberProcess").click(function(){
		var data = $('#form').serialize();
		var request = $.ajax({
		url: "member_process.php",
		data: data,
		type: "POST",
		dataType: "json" });
		request.done(function(msg) {
		alert("mem_id" + msg['mem_id'] + " uid=" + msg['uidMsg'] + " Status=" + msg['status'] + " action=" + msg['action_type']);
		//alert(msg['mem_id']+" has been added.");
			
			$( "#divEditMember" ).hide();
                        
			if(msg['status']=="Success"){
				document.getElementById('idMemUID').innerHTML = "<b>รหัสสมาชิก : </b>" + msg['uidMsg'];

				$( "#divEditMemberStatusSuccess" ).show();
				
			}else{
				$( "#divEditMemberStatusFail" ).show();
			}
			iframe_search.document.location.href = "member_searchIframe1.php?type=id&keyword=0";
			iframe_member.document.location.href = "member_memberiframe.php";
		});
		request.fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
		});
	});
	
	$("#deleteMemberProcess").click(function(){

                $( "#memDetailDelete" ).show();
                
		var userData = 'action_type=delete&mem_id=' + $('#mem_id').val() + '&uidMsg=';
		var request = $.ajax({
		url: "member_process.php",
		data: userData,
		type: "POST",
		dataType: "json" });
		request.done(function(msg) {
		//alert("mem_id" + msg['mem_id'] + " uid=" + msg['uidMsg'] + " Status=" + msg['status'] + " action=" + msg['action_type']);
		//alert(msg['mem_id']+" has been delete." + " action=" + msg['action_type']);
			$( "#deletemodalControl" ).hide();
                        $( "#memDetailDelete" ).hide();
                        
			if(msg['status']=="Success"){
				document.getElementById('idMemUID').innerHTML = "<b>รหัสสมาชิก : </b>" + msg['uidMsg'];
				$( "#divDeleteMemberStatusSuccess" ).show();
				
			}else{
				$( "#divDeleteMemberStatusFail" ).show();
			}
                        
			iframe_search.document.location.href = "member_searchmainIframe.php";
			iframe_member.document.location.href = "member_memberiframe.php";
		});
		request.fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
		});
		
	});
});
function divCloseMemEdit() {
    $( "#divEditMember" ).show();
	$( "#divEditMemberStatusSuccess" ).hide();
	$( "#divEditMemberStatusFail" ).hide();
	$('#modify').modal('hide');
}

function divCloseMemDelete() {
    $( "#deletemodalControl" ).show();
	$( "#divDeleteMemberStatusSuccess" ).hide();
	$( "#divDeleteMemberStatusFail" ).hide();
	$( '#deletemodal' ).modal('hide');
}

function myFunction() {
    alert("I am an alert box!");
	$('#modify').modal('hide');
	$(deletemodal).modal();

}

function clearInputUidEdit(){
   document.getElementById("uidMsg").value = '';
}
</script>
	