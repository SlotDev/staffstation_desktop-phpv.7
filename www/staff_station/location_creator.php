<div class="row">
	<div class="col-sm-4 col-lg-3">
		<div class="panel panel-primary" style="height:95%;">
			<div class="panel-body">
				<span class="pull-left"><?php echo $lang_locate; ?> </span><a href="head_location.php?page=location_creator"><span class="glyphicon glyphicon-list pull-right"></span></a>
				<hr>
				<iframe id="iframe_lo" name="iframe_lo" src="location_levelIframe.php" style="width:100%;height:85%;border:0px;"></iframe>

			</div>
		</div>
	</div>
	<script type="text/javascript">
		add = function(){
        	iframe_lo.document.location.href = "location_levelIframe.php";
		}
		function select_level(x){
        	iframe_add.document.location.href = "location_onelevelIframe.php?&page=area_info&id="+x;
		}

		function modify1(id1,level_name1){
			document.getElementById("id").value = id1;
			document.getElementById("level_name").value = level_name1;
			$(modify).modal();
		}
		function move1(id_move,level_name_move){
			document.getElementById("id_move").value = id_move;
			document.getElementById("level_name_move").value = level_name_move;
			$(move).modal();
		}
		function move_book(tag_move_book){
			document.getElementById("tag_id").value = tag_move_book;
			document.getElementById("tag_id_1").value = tag_move_book;
			$(move_book1).modal();
		}
	</script>

	<div class="col-sm-8 col-lg-9">
  		<div class="panel panel-primary" style="height:95%;">
			<div class="panel-body">
					<iframe id="iframe_add" name="iframe_add" src="location_addIframe.php" style="width:100%;height:95%;border:1px;"></iframe>
	    	</div>
	    </div>
	</div>
</div>

	<!-- Modal modify-->
	<div class="modal fade " id="modify" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"><?php echo $lang_rename; ?></h4>
	      </div>
	      <div class="modal-body">
	      	<div class="panel panel-primary">
	  			<div class="panel-body">
				    <div align="center">
				    	<form class="form-horizontal" method="post">
				    		<div class="form-group">
								<label class="col-sm-4 control-label"><?php echo $lang_old_name; ?></label>
								<div class="col-sm-6">
									<input type="hidden" id="id" name="id" class="form-control" >
									<input type="text" id="level_name" name="level_name" class="form-control" disabled="true">
								</div>
							</div>
				    		<div class="form-group">
							    <label class="col-sm-4 control-label"><?php echo $lang_new_name; ?></label>
							    <div class="col-sm-6">
							    	<input type="text" name="new_name" data-validation="required" class="form-control" >
							    </div>
							</div>
							<hr>
					    	<p class="lead"><?php echo $lang_submit_rename; ?></p>
					      	<button type="submit" name="submit_new_name" class="btn btn-link" style="outline-style:none;"><img src="img/correct.png" style="width:60%;" onmouseover="this.src='img/correct_1.png'"onmouseout="this.src='img/correct.png'"></button>
							<a href="#" data-dismiss="modal"><img src="img/incorrect.png" style="width:10%;" onmouseover="this.src='img/incorrect_1.png'"onmouseout="this.src='img/incorrect.png'"></a>
				    	</form>
				    	
				    </div>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal move-->
	<div class="modal fade" id="move" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"><?php echo $lang_move_locate; ?></h4>
	      </div>
	      <div class="modal-body">
	      	<div class="panel panel-primary">
	  			<div class="panel-body">
				    <div align="center">
				    	<form class="form-horizontal" method="post">
				    		<div class="form-group">
								<label class="col-sm-4 control-label"><?php echo $lang_old_locate; ?></label>
								<div class="col-sm-6">
									<input type="hidden" id="id_move" name="id_move" class="form-control" >
									<input type="hidden" id="level_name" name="level_name" class="form-control">
									<input type="text" id="level_name_move" name="level_name" class="form-control" disabled="true">
								</div>
							</div>
							<hr>
							<?php 
								$i=0;
								$sql_move = "SELECT location_name,id,parent_id FROM locations";
								$query_move = mysqli_query($conn,$sql_move);
								$row_move = mysqli_num_rows($query_move);
							?>
				    		<div class="form-group">
							    <label class="col-sm-4 control-label"><?php echo $lang_new_locate; ?></label>
								    <div class="col-sm-8" align="left">
								    	<div class="row">
								    		<div class="col-sm-6">
										    	<div class="radio">
										    		<label>
											    		<input type="radio" name="new_location" id="new_location" value="0">location หลัก<br>
													</label>
											    </div>
										    </div>
									    <?php
								    		while($res_move = mysqli_fetch_array($query_move,MYSQLI_BOTH)){
								    	?>
										    <div class="col-sm-6">
										    	<div class="radio">
										    		<label>
											    		<input type="radio" name="new_location" id="new_location" value="<?php echo $res_move['id']; ?>"><?php echo $res_move['location_name']; ?><br>
													</label>
											    </div>
										    </div>
									    <?php } ?>
								    	</div>
								    </div>
							</div>
							<hr>
							<div align="center">
						    	
						      	<button type="submit" name="submit_new_location" class="btn btn-link" style="outline-style:none;"><img src="img/correct.png" style="width:60%;" onmouseover="this.src='img/correct_1.png'"onmouseout="this.src='img/correct.png'"></button>
								<a href="#" data-dismiss="modal"><img src="img/incorrect.png" style="width:10%;" onmouseover="this.src='img/incorrect_1.png'"onmouseout="this.src='img/incorrect.png'"></a>
				    		</div>
				    	</form>
				    	
				    </div>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal move_book-->
	<div class="modal fade" id="move_book1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">ย้ายหนังสือ</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="panel panel-primary">
	  			<div class="panel-body">
				    <div align="center">
				    	<form class="form-horizontal" method="post">
				    		<div class="form-group">
					    		<label class="col-sm-4 control-label">รหัสหนังสือ</label>
								<div class="col-sm-8" align="left">
									<input type="text" id="tag_id" class="form-control" disabled="true">
									<input type="hidden" id="tag_id_1" name="tag_id_1" class="form-control">
								</div>
							</div>
							<hr>
							<?php 
								$i=0;
								$sql_move = "SELECT location_name,id,parent_id FROM locations";
								$query_move = mysqli_query($conn,$sql_move);
								$row_move = mysqli_num_rows($query_move);
							?>
				    		<div class="form-group">
							    <label class="col-sm-4 control-label">location ใหม่</label>
								    <div class="col-sm-8" align="left">
								    	<div class="row">
									    <?php
								    		while($res_move = mysqli_fetch_array($query_move,MYSQLI_BOTH)){
								    	?>
										    <div class="col-sm-6">
										    	<div class="radio">
										    		<label>
											    		<input type="radio" name="new_location" id="new_location" value="<?php echo $res_move['id']; ?>"><?php echo $res_move['location_name']; ?><br>
													</label>
											    </div>
										    </div>
									    <?php } ?>
								    	</div>
								    </div>
							</div>
							<hr>
							<div align="center">
						      	<button type="submit" name="submit_new_location_book" class="btn btn-link" style="outline-style:none;"><img src="img/correct.png" style="width:60%;" onmouseover="this.src='img/correct_1.png'"onmouseout="this.src='img/correct.png'"></button>
								<a href="#" data-dismiss="modal"><img src="img/incorrect.png" style="width:10%;" onmouseover="this.src='img/incorrect_1.png'"onmouseout="this.src='img/incorrect.png'"></a>
				    		</div>
				    	</form>
				    	
				    </div>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>


<?php 
	if(isset($_POST['submit_new_name'])){
		$new_name = $_POST['new_name'];
		$id = $_POST['id'];
		echo $sql_update = "UPDATE locations SET location_name = '$new_name' WHERE id = '$id'";
		$query_update = mysqli_query($conn,$sql_update);
		echo "<META http-equiv=\"REFRESH\" content=\"0;url=head_location.php?page=location_creator\">";
	}

	if(isset($_POST['submit_new_location'])){
		$id_move = $_POST['id_move'];
		$new_location = $_POST['new_location'];
		$update_move = "UPDATE locations SET parent_id = '$new_location' WHERE id = '$id_move'";
		$query_update_move = mysqli_query($conn,$update_move);
	}

	if(isset($_POST['submit_new_location_book'])){
		$cus_id = $_SESSION['cus_id'];
		$tag_id = $_POST['tag_id_1'];
		$new_location = $_POST['new_location'];
		$update_move = "UPDATE inventory SET position_id = '$new_location',date = now(),customer_id = '$cus_id' WHERE tag_id = '$tag_id'";
		$query_update_move = mysqli_query($conn,$update_move);
	}
?>