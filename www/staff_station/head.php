<?php 

  include "connect_db.php";
  $lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    $_SESSION['lang'] = $_GET['lang']; //เก็บค่าของภาษาไว้ใน SESSION
      if($_SESSION['lang'] == "eng"){
        include "lang_eng.php";
      }
      else{
        include "lang_th.php";
      }
    }
    else if ($_SESSION['lang'] == 'eng') {
      include "lang_eng.php";
    }
    else{
      include "lang_th.php";
    }
    $num = 0;
    $link_function = '';

?>
<html>
 <head>
   <meta http-equiv=Content-Type content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="jquery-form-validator/jquery.min.js"></script>
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/hint.css">
  <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>

</head>

<body>
<div class="container-fliud">
     <div class="panel panel-primary" style="border-radius: 0;height:11%;background:#2e7ed0;" >
      <div class="panel-body">
        <div class="row">
        	<div class="col-xs-6 col-sm-4 col-lg-4" align="left">
        		<img src="img/logo.png" width="50px">
        	</div>
        	<div class="hidden-xs col-sm-4 col-lg-4" align="center">
        		<p class="text-uppercase">
              <?php if(strlen($orga) < 15){ ?>
                <font color="#ffffff" size="6"><?php echo $orga; ?></font>
              <?php }else{ ?>
                <font color="#ffffff" size="5"><?php echo $orga; ?></font>
              <?php } ?>
            </p>
        	</div>
        	<div class="col-xs-6 col-sm-4 col-lg-4" align="right">
        		<a href="?lang=th"><img src="img/flag_th.png" width="30px"></a>
            <a href="?lang=eng"><img src="img/flag_en.png" width="30px"></a>
        		<p><font color="#ffffff" size="2"><strong><?php echo $name; ?>&nbsp;  </strong></font><a href="logout.php"><strong><u><font color="#ffffff" size="1">  <?php echo $lang_logout; ?></font></u></strong></a></p>
        	</div>
        </div>
      </div>
    </div>
   <div class="panel panel-primary" style="height:83%;">
    <div class="panel-body">
        <div class="row">
          <div class="hidden-xs col-sm-12">&nbsp;</div>
          <div class="hidden-xs col-sm-12">&nbsp;</div>
          <div class="hidden-xs col-sm-12">&nbsp;</div>
          <div class="hidden-xs col-sm-12">&nbsp;</div>
          <div class="hidden-xs col-sm-12">&nbsp;</div>
        </div>

    <table align="center" style="border:0;">
      <tr>
        <?php if($function_circulation == 1){ ?>
          <td width="300px" align="center">
            <div class="hint--bottom hint--bounce hint--menu1" data-hint="<?php echo $lang_menu1; ?>">
              <a href="head_circulation.php?page=circulation"  ><img src="img/menu1.png" style="width:150%"></a>
            </div>
          </td>
        <?php }if($function_member == 1){ ?>
          <td width="300px" align="center">
            <div class="hint--bottom hint--bounce hint--menu2" data-hint="<?php echo $lang_menu2; ?>">
              <a href="head_member.php?page=member_search"><img src="img/menu2.png" style="width:150%"></a>
            </div>
          </td>
        <?php }if($function_tagdata == 1){ ?>
          <td width="300px" align="center">
            <div class="hint--bottom hint--bounce hint--menu3" data-hint="<?php echo $lang_menu3; ?>">
              <a href="head_tagData.php?page=tag_main"><img src="img/menu3.png" style="width:150%"></a>
            </div>
          </td>
        <?php }if($function_location == 1){ ?>
          <td width="300px" align="center">
            <div class="hint--bottom hint--bounce hint--menu4" data-hint="<?php echo $lang_menu4; ?>">
              <a href="head_location.php?page=location_creator"><img src="img/menu4.png" style="width:150%" ></a>
            </div>
          </td>
        <?php } ?>
      </tr>
    </table>
    </div>
  </div>
</div>

 </body>

 </html>
