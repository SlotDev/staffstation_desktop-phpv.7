<?php
//----------login.php-------------//
$lang_login = "Login";
$lang_username = "Username";
$lang_password = "Password";
$lang_validate_user = "Enter Your're Username";
$lang_validate_pass = "Enter Your're Password";

//----------head.php-------------//
$lang_menu1 = "Circulation";
$lang_menu2 = "Member";
$lang_menu3 = "Tag Data";
$lang_menu4 = "Location";
$lang_logout = "logout";

//----------head_circulation.php-------------//
$lang_member_card = "Member Name or ID";
$lang_enter = "Enter";
$lang_validate = "You have not answered all required fields";

//----------circulation_main.php-------------//
$lang_member = "MEMBER";
$lang_patron_id = "Patron ID";
$lang_card_id = "Card ID";
$lang_borrow = "Borrow";
$lang_booking = "Booking";
$lang_over = "Over Date";
$lang_remark = "*เงินคงเหลือไม่พอจ่ายค่าปรับ กรุณาเติมเงิน*";

//----------circulation_functionIframe.php-------------//
$lang_function = "Select Function";
$lang_func_borrow = "Borrow";
$lang_func_renew = "Renew";
$lang_func_return = "Return";

//----------tag_main.php-------------//
$lang_encode = "Encode Tag";
$lang_read = "Read Tag";
$lang_msg_barcode = "Please Enter Barcode";
$lang_barcode = "Enter Barcode";
$lang_call_no = "Call No.";
$lang_book_name = "Book Name";
$lang_author = "Author";
$lang_status = "Book Status";
$lang_security = "Security Status";
$lang_tag_id = "Tag ID";
$lang_create = "Create Tag";
$land_detail_book = "Detail Book";

$lang_available = "On Shelf";
$lang_unavailable = "Unavailable";
$lang_tag_false = "tag false";

//----------tag_readIframe.php-------------//
$lang_not_found_tag = "Not Found Tag On Staff Station";
$lang_not_found_book = "[ENG]รหัส tag ที่พบ ไม่ตรงกับข้อมูลหนังสือ";

//----------tag_encodeIframe.php-------------//
$lang_not_found_barcode = "[ENG]ไม่พบรหัสบาร์โค้ดนี้ในระบบห้องสมุด";

//----------location_creator.php------------//
$lang_locate = "Location Builder";
$lang_rename = "Rename location";
$lang_old_name = "Old Name";
$lang_new_name = "New Name";
$lang_submit_rename = "Do you want to Rename";
$lang_move = "Move Location";
$lang_old_locate = "[ENG] location เ่กีา";
$lang_new_locate = "[ENG] location ใหม่";

//----------location_levelIframe.php------------//
$lang_expand = "Expand All";
$lang_collapse = "Collapse All";

//----------location_addIframe.php------------//
$lang_location_main = "Location Main";
$lang_addby = "Add by";
$lang_name = "Name";
$lang_add = "Add";
$lang_delete = "Delete";
$lang_no = "No.";
$lang_id = "ID";
$lang_location_name = "Location Name";
$lang_edit = "Edit";
$lang_move = "Move";
$lang_add_locate = "Add location";
$lang_data_locate = "Data location";

//----------location_addIframe.php------------//
$lang_book_info = "ข้อมูลหนังสือใน location";
$lang_book_add = "เพิ่มหนังสือใน location";

//----------book_add.php------------//
$lang_submit_add = "[ENG]ยืนยันการเพิ่ม";

//----------area_info.php------------//
$lang_add_locate_name = "Add Location Name";

//----------book_info.php------------//
$lang_user_id = "User ID";
$lang_date = "Date";

//----------head_member.php------------//
$lang_orga = "PCCCR Library";

?>