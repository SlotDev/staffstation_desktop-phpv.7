<?php 
	include "connect_db.php";
	include("api_hongkhai/nusoap.php");

	$client = new nusoap_client($path_api,true); 

	$lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    	$_SESSION['lang'] = $_GET['lang']; //เก็บค่าของภาษาไว้ใน SESSION
      	if($_SESSION['lang'] == "eng"){
        	include "lang_eng.php";
      	}
      	else{
        	include "lang_th.php";
      	}
    }
    else if ($_SESSION['lang'] == 'eng') {
      	include "lang_eng.php";
    }
    else{
      	include "lang_th.php";
    } 

?>
<html lang="en">
 <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
 </head>

<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<br><br>
			<center><img src="img/loader_blue.gif" style="width:30%;"></center>
		</div>
	</div>
</div>
</body>

</html>
