<?php

function callTraceoNService($host="127.0.0.1", $port=8088, $timeout=30, $command)
{
error_reporting(E_ALL);

ob_implicit_flush();

/* Get the port for the WWW service. */
$service_port = $port;

/* Get the IP address for the target host. */
$address = $host;

/* Create a TCP/IP socket. */
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}
socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array("sec" => $timeout, "usec" =>0));
//echo "Attempting to connect to '$address' on port '$service_port'...";
$result = socket_connect($socket, $address, $service_port);
if ($result === false) {
    echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
}

$in = "FRMWEB:" . $command;
$out = '';
//echo $in;
//$byte_array = unpack('C*', $in);
//var_dump($byte_array);
//echo $byte_array;

//echo "Sending request...";
socket_write($socket, $in, strlen($in));

sleep(1);
//echo "Reading response:\n\n";
$out = socket_read($socket, 2048);
//echo $out;

//echo "Closing socket...";
socket_close($socket);

return $out;

}
?>