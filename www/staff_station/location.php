
<?php 
  if(!isset($_SESSION)){
      session_start();
  }
  include "connect_db.php";
?>
<html lang="en">
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="jquery-form-validator/jquery.min.js"></script>
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
  <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
  <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>

</head>

<body>
<div class="container-fliud">
     <div class="panel panel-primary" style="height:11%;">
      <div class="panel-body">
        <div class="row">
        	<div class="col-sm-4 col-lg-4" align="left">
        		<img src="img/logo.png" width="50px">
        	</div>
        	<div class="col-sm-4 col-lg-4" align="center">
        		<p class="lead text-primary">Hong-Khai Library</p>
        	</div>
        	<div class="col-sm-4 col-lg-4" align="right">
        		<a href="head.php"><img src="img/home.ico" width="30px"></a>
        		<img src="img/flag_th.png" width="30px">
        		<img src="img/flag_en.png" width="30px">
        		<h6><?php echo $username; ?> <a href="logout.php"><strong><u>ออกจากระบบ</u></strong></a></h6>
        	</div>
        </div>
      </div>
    </div>
   <div class="panel panel-primary" style="height:83%;">
    <div class="panel-body">
  	<div class="container">
      <div class="row">
      <div class="col-sm-12 col-lg-12">&nbsp;</div>
      <div class="col-sm-12 col-lg-12">&nbsp;</div>
      <div class="col-sm-12 col-lg-12">&nbsp;</div>
      <div class="col-sm-4 col-lg-4">
        <a href="head_location.php?page="><img src="img/locate1.png" width="100%"></a>
      </div>
      <div class="col-sm-4 col-lg-4">
        <a href="head_location.php?page="><img src="img/locate2.png" width="100%"></a>
      </div>
      <div class="col-sm-4 col-lg-4">
        <a href="head_location.php?page="><img src="img/locate3.png" width="100%"></a>
      </div>
        </div>
      </div>
    </div>
  </div>
</div>
 <script>
	 $.validate({
		 modules: 'security, file',
		 onModulesLoaded: function () {
		 	$('input[name="pass_confirmation"]').displayPasswordStrength();
		 }
	 });
 </script>
 </body>

 </html>
