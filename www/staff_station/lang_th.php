<?php
//----------login.php-------------//
$lang_login = "เข้าสู่ระบบ";
$lang_username = "ชื่อผู้ใช้";
$lang_password = "รหัสผ่าน";
$lang_validate_user = "กรอกชื่อผู้ใช้";
$lang_validate_pass = "กรอกรหัสผ่าน";

//----------head.php-------------//
$lang_menu1 = "ระบบยืม-คืน";
$lang_menu2 = "ระบบสมาชิก";
$lang_menu3 = "ระบบเข้ารหัสหนังสือ";
$lang_menu4 = "ระบบตำแหน่งหนังสือ";
$lang_logout = "ออกจากระบบ";

//----------head_circulation.php-------------//
$lang_member_card = "ใส่รหัสมาชิก หรือรหัสบัตรสมาชิก";
$lang_enter = "ตกลง";
$lang_validate = "กรุณากรอกข้อมูล";

//----------circulation_main.php-------------//
$lang_member = "สมาชิก";
$lang_patron_id = "รหัสสมาชิก";
$lang_card_id = "รหัสการ์ด";
$lang_borrow = "จำนวนที่ยืม";
$lang_booking = "จำนวนที่จอง";
$lang_over = "จำนวนที่เกิน";
$lang_remark = "*เงินคงเหลือไม่พอจ่ายค่าปรับ กรุณาเติมเงิน*";

//----------circulation_functionIframe.php-------------//
$lang_function = "เลือกฟังก์ชั่น";
$lang_func_borrow = "ยืม";
$lang_func_renew = "ยืมต่อ";
$lang_func_return = "คืน";

//----------tag_main.php-------------//
$lang_encode = "เข้ารหัสหนังสือ";
$lang_read = "อ่านรหัสหนังสือ";
$lang_msg_barcode = "ใส่รหัสบาร์โค้ดก่อนทำรายการ";
$lang_barcode = "รหัสบาร์โค้ด";
$lang_call_no = "เลขเรียกหนังสือ";
$lang_book_name = "ชื่อหนังสือ";
$lang_author = "ชื่อผู้แต่ง";
$lang_status = "สถานะหนังสือ";
$lang_security = "สถานะความปลอดภัย";
$lang_tag_id = "รหัส tag";
$lang_create = "สร้างรหัส tag";
$land_detail_book = "รายละเอียดหนังสือ";

$lang_available = "หนังสืออยู่บนชั้น";
$lang_unavailable = "หนังสือถูกยืมอยู่";
$lang_tag_false = "รหัส tag ไม่ถูกต้อง";

//----------tag_readIframe.php-------------//
$lang_not_found_tag = "ไม่พบ tag บนเครื่องอ่าน";
$lang_not_found_book = "รหัส tag ที่พบ ไม่ตรงกับข้อมูลหนังสือ";

//----------tag_encodeIframe.php-------------//
$lang_not_found_barcode = "ไม่พบรหัสบาร์โค้ดนี้ในระบบห้องสมุด";

//----------location_creator.php------------//
$lang_locate = "รายการที่ตั้ง";
$lang_rename = "เปลี่ยนชื่อ location";
$lang_old_name = "ชื่อเก่า";
$lang_new_name = "ชื่อใหม่";
$lang_submit_rename = "ยืนยันการเปลี่ยนชื่อใหม่";
$lang_move_locate = "ย้าย location";
$lang_old_locate = "location เดิม";
$lang_new_locate = "location ใหม่";

//----------location_levelIframe.php------------//
$lang_expand = "แสดงทั้งหมด";
$lang_collapse = "ซ่อนทั้งหมด";

//----------location_addIframe.php------------//
$lang_location_main = "ที่ตั้งหลัก";
$lang_addby = "เพิ่มโดย";
$lang_name = "ชื่อ";
$lang_add = "เพิ่ม";
$lang_delete = "ลบ";
$lang_no = "ลำดับ";
$lang_id = "id";
$lang_location_name = "ชื่อสถานที่";
$lang_edit = "แก้ไข";
$lang_move = "ย้าย";
$lang_add_locate = "เพิ่ม location";
$lang_data_locate = "ข้อมูล location";

//----------location_addIframe.php------------//
$lang_book_info = "ข้อมูลหนังสือใน location";
$lang_book_add = "เพิ่มหนังสือใน location";

//----------book_add.php------------//
$lang_submit_add = "ยืนยันการเพิ่ม";

//----------area_info.php------------//
$lang_add_locate_name = "เพิ่มชื่อ location";

//----------book_info.php------------//
$lang_user_id = "รหัส user";
$lang_date = "วันที่";

//----------head_member.php------------//
$lang_orga = "ห้องสมุดเฉลิมพระเกียรติกาญจนาภิเษก";


?>