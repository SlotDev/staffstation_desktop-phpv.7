<div class="row">
	<div class="col-sm-4 col-lg-3">
		<div class="panel panel-primary" style="height:95%;">
			<div class="panel-body">
				<span class="pull-left text-info"><strong><h3><?php echo $lang_encode; ?></h3></strong></span>
				<br><br><hr>
				<form method="post" target="iframe_tag" name="form_encode" id="form_encode" action="tag_encodeIframe.php">
					<div class="form-group">
						<input type="text" id="write_tag" name="write_tag" autofocus="autofocus" class="form-control" data-validation="required" data-validation-error-msg="<?php echo $lang_msg_barcode; ?>" placeholder="<?php echo $lang_barcode; ?>" >
					</div>
					<div align="center">
						<button type="submit" class="btn btn-link" data-toggle="tooltip" style="outline-style:none;" data-placement="right" title="encode" onclick='endcode_tag();'>
							<img src="img/encode.png" style="width:140%" onmouseover="this.src='img/encode_1.png'"onmouseout="this.src='img/encode.png'">
						</button>
					</div>
				</form>
				<br>
				<span class="pull-left text-info"><strong><h3><?php echo $lang_read; ?></h3></strong></span>
				<br><br><hr>
				<div align="center">
					<button type="button" class="btn btn-link" data-toggle="tooltip" style="outline-style:none;" data-placement="right" title="read tag" onclick='read_tag();'>
						<img src="img/readtag.png" style="width:140%;" onmouseover="this.src='img/readtag_1.png'"onmouseout="this.src='img/readtag.png'">
					</button>
				</div>
				<!--<form method="post" target="iframe_tag" action="tag_readIframe.php" align="center" id="form_tag" name="form_tag">-->
					<input type="hidden" id="addTagBorrow" name="addTagBorrow">
					
				<!--</form>
				<hr>
				<form method="post" target="iframe_tag" action="exceptionIframe.php" align="center">
					<input class="btn btn-primary btn-lg btn-block" type="submit" value="เพิ่มหนังสือที่ได้รับการยกเว้น">
				</form>-->

			</div>
		</div>
	</div>
	<div class="col-sm-8 col-lg-9">
  		<div class="panel panel-primary" style="height:95%;">
			<div class="panel-body">
				<iframe id="iframe_tag" name="iframe_tag" style="width:100%;height:90%;border:none; "></iframe>
			</div>
	    </div>
	</div>
</div>
<iframe id="ifraRegor" src="about:blank;" style="width:0px;height:0px;border:none;"></iframe>
<script type="text/javascript">
		var timer;
		var tmr = 5000;

		function read_tag(){
			document.getElementById("write_tag").value="";
			document.getElementById('iframe_tag').src = 'tag_loadIframe.php';
			
			document.getElementById("addTagBorrow").value="";
			controlRegor("readstart","3");
			//setTimeout(function(){
				//controlRegor('readstop','0');
			//}, 5000);
		}
		function endcode_tag(){
			document.getElementById('iframe_tag').src = 'tag_blankIframe.php';
		}

		function controlRegor(command, times)
		{
			
			document.getElementById('ifraRegor').src = "about:blank;";
			document.getElementById('ifraRegor').src = "staff/LiveRegor_read.php?command="+command+"&timer="+times;
		}

		function controlRegor_write(command, times)
		{
			document.getElementById('ifraRegor').src = "about:blank;";
			document.getElementById('ifraRegor').src = "staff/LiveRegor_write.php?command="+command+"&timer="+times;
		}

		function checkFatalErr()
		{
			if(document.getElementById("ifraRegor").contentWindow.document.body.innerHTML.indexOf("Fatal error:")>0)
			{
				document.getElementById("ifraRegor").src=document.getElementById("ifraRegor").src;
			}
			setTimeout("checkFatalErr();",8000);
		}
		
		function tag_read(out){
			document.getElementById("addTagBorrow").value+= out+"|";
		}

		function show_tag_read(){
			document.getElementById('iframe_tag').src = 'tag_readIframe.php?out='+document.getElementById("addTagBorrow").value;
		}

		var validation_length1 = <?php echo json_encode($validation_length1); ?>;
		var validate_encode = '';
		function abc(tag,img,bc,call,name,chk){
			
			var lang_available = <?php echo json_encode($lang_available); ?>;
			var lang_unavailable = <?php echo json_encode($lang_unavailable); ?>;
        	document.getElementById("bc").innerHTML = bc;
        	document.getElementById("tag").innerHTML = tag;
        	document.getElementById("call").innerHTML = call;
        	document.getElementById("name").innerHTML = decodeURI(name);
        	//document.getElementById("auther").innerHTML = auther;
			//document.getElementById("lib_location").innerHTML = liblocation;
        	document.getElementById("imgBook").src = img;
        	if(chk == 'false'){
        		document.getElementById("chk").innerHTML = "<font color=green>"+lang_available+"</font>";
        	}
        	else{
        		document.getElementById("chk").innerHTML = "<font color=red>"+lang_unavailable+"</font>";
        	}
        	$(popupid).modal();
		}
		/*function encode1(tag1,img1,bc1,call1,name1,auther1,chk1,en1,liblocation1){
			
			var lang_available1 = <?php echo json_encode($lang_available); ?>;
			var lang_unavailable1 = <?php echo json_encode($lang_unavailable); ?>;
			validate_encode = en1;
        	document.getElementById("bc1").innerHTML = bc1;
        	document.getElementById("imgBook1").src = img1;
        	if(tag1==0){
        		document.getElementById("sec1").innerHTML = "<font color=red>Disabled</font>";
        	}
        	else{ 
        		document.getElementById("sec1").innerHTML = "<font color=green>Enabled</font>";
        	}
        	document.getElementById("name1").innerHTML = name1;
        	document.getElementById("call1").innerHTML = call1;
        	document.getElementById("encode_name").value = name1;
        	document.getElementById("auther1").innerHTML = auther1;
			document.getElementById("lib_location1").innerHTML = liblocation1;
        	if(chk1 == 'true'){
        	document.getElementById("chk1").innerHTML = "<font color=green>"+lang_available1+"</font>";
        	}
        	else if(chk1 != 'true'){
        	document.getElementById("chk1").innerHTML = "<font color=red>"+lang_unavailable1+"</font>";
        	}
        	$(createtag1).modal();

        	setTimeout("document.getElementById('encode_tag1').focus();",1000);
		}*/

		function tag_write(write){
			document.getElementById("addTagBorrow").value+= write+"|";
			document.getElementById('iframe_tag').src = 'tag_statusIframe.php?encode_tag1='+validate_encode+'&call_no='+call_no_s+'&status='+document.getElementById("addTagBorrow").value+'&encode_name='+encodeURIComponent(name_book_s);
			document.getElementById('encode_tag1').value='';
		}
		var validate_encode,name_book_s,call_no_s;

		function validate_tag(barcode_2_key,barcode_1_key,name_key,call_key){
			var lang_tag_false1 = <?php echo json_encode($lang_tag_false); ?>;
			//if(barcode_2_key.length == validation_length1){
				if(barcode_2_key == barcode_1_key){
				    //$('#createtag1').modal('hide');
					validate_encode = barcode_2_key;
					name_book_s = name_key;
					call_no_s = call_key;
				    controlRegor_write("write",validate_encode);
				    //setTimeout("document.getElementById('encode_tag1').value='';",1000);
				}
				else{
					alert(lang_tag_false1);
					document.getElementById('tag_false').innerHTML = "<font color=red>"+lang_tag_false1+"</font>";
					return false;
				}
			//}
			document.getElementById("addTagBorrow").value="";
		}

		function validate_tag_onkeyup(barcode_2_on,barcode_1_on,name_on,call_on){
			var lang_tag_false2 = <?php echo json_encode($lang_tag_false); ?>;
			
			//if(event.keyCode=='13'){
				//if(barcode_2_on.length == validation_length1){
					if(barcode_2_on == barcode_1_on){
					    //$('#createtag1').modal('hide');
						validate_encode = barcode_2_on;
						name_book_s = name_on;
						call_no_s = call_on;
					    controlRegor_write("write",validate_encode);
					    //setTimeout("document.getElementById('encode_tag1').value='';",1000);
					}
					else{
						alert(lang_tag_false2);
						document.getElementById('tag_false').innerHTML = "<font color=red>"+lang_tag_false2+"</font>";
						return false;
					}
				//}
			//}
			document.getElementById("addTagBorrow").value="";
		}
		
		

</script>
<!-- Modal create tag 1-->
<div class="modal fade" id="createtag1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $lang_encode; ?></h4>
      </div>
      <div class="modal-body">
    		<div class="row">
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-10">
					<form class="form-inline" method="post" name="modal_encode"  onsubmit="return false;">
						<div class="panel panel-primary" style="width:100%;">
							<div class="panel-body">
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-4" align="center">
									<img id="imgBook1" style="width:150px">
								</div>
								<div class="col-md-8">
									<table class="table">
										<tr>
											<th>Check <?php echo $lang_barcode; ?></th>
											<td id="bc1"></td>
										</tr>
										<tr>
											<th><?php echo $lang_call_no; ?></th>
											<td id="call1"></td>
											<input type="hidden" id="call_no" name="call_no">
										</tr>
										<tr>
											<th><?php echo $lang_book_name; ?></th>
											<td id="name1"></td>
											<input type="hidden" id="encode_name" name="encode_name">
										</tr>
										<tr>
											<th><?php echo $lang_author; ?></th>
											<td id="auther1"></td>
										</tr>
										<tr>
										<th><?php echo $lang_location; ?></th>
											<td id="lib_location1"></td>
										</tr>
										<tr>
											<th><?php echo $lang_status; ?></th>
											<td id="chk1"></td>
										</tr>
										<tr>
											<th><?php echo $lang_security; ?></th>
											<td id="sec1"></td>
										</tr>
									</table><br>
									<div class="form-group">
										<label><?php echo $lang_tag_id; ?></label>
										<input type="text" autofocus class="form-control" id="encode_tag1" name="encode_tag1" data-validation="alphanumeric length" data-validation-length="<?php echo $validation_length; ?>" data-validation-error-msg="<?php echo $lang_validate; ?>" onkeyup="validate_tag_onkeyup();">
										<div id="tag_false"></div>
									</div>
								</div>
							</div>
						</div>
						<div align="center">
					        <button type="button" onclick="validate_tag();" name="submit_encode" class="btn btn-link" style="outline-style:none;"><img src="img/correct.png" onmouseover="this.src='img/correct_1.png'" onmouseout="this.src='img/correct.png'" style="width:65%;"></button>
					        &nbsp;
							<a href="#" data-dismiss="modal"><img src="img/incorrect.png" onmouseover="this.src='img/incorrect_1.png'"onmouseout="this.src='img/incorrect.png'"style="width:8%;"></a>
						</div>
					</form>
				</div>
			</div>
      	</div>
    </div>
  </div>
</div>


<!-- Modal detailbook-->
<div class="modal fade" id="popupid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $land_detail_book; ?></h4>
      </div>
      <div class="modal-body">
    		<div class="row">
				
				<div class="col-md-12">
					<div class="panel panel-primary" style="width:100%;">
						<div class="panel-body">
							<div class="col-md-12">&nbsp;</div>
							<div class="col-md-4" align="center">
								<img id="imgBook" style="width:150px">
							</div>
							<div class="col-md-8">
								<table class="table">
									<tr>
										<th><?php echo $lang_barcode; ?></th>
										<td id="bc"></td>
									</tr>
									<tr>
										<th><?php echo $lang_tag_id; ?></th>
										<td id="tag"></td>
									</tr>
									<tr>
										<th><?php echo $lang_call_no; ?></th>
										<td id="call"></td>
									</tr>
									<tr>
										<th><?php echo $lang_book_name; ?></th>
										<td id="name"></td>
									</tr>
									<tr>
										<th><?php echo $lang_status; ?></th>
										<td id="chk"></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div align="center">
		        <a href="#" data-dismiss="modal"><img src="img/correct.png" style="width:8%;"></a>
			</div>
      	</div>
    </div>
  </div>
</div>

