﻿<?php 
  if(!isset($_SESSION)){
      session_start();
  }
?>

<html lang="en">
 <head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css" >
	<link href="bootstrap-3.3.5-dist/css/TableFixedHeader.css" rel="stylesheet">
        <link href="bootstrap-3.3.5-dist/css/checkbox.css" rel="stylesheet">
        <link href="bootstrap-3.3.5-dist/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
	<script>
	var varItemNo = 1;
	
	function addRowReturnList2(tableID,rowBookImage,rowBookTitle,rowBookStatus) {
		
	  var tableRef = document.getElementById(tableID);
	  var mem_id = top.document.getElementById("mem_id").value;
	  var uidMsg = top.document.getElementById("uidMsg").value;
	  var mem_name = top.document.getElementById("mem_name").value;
	  
	  var img = document.createElement("img");
	  img.width = "70";
	  img.height = "70";
	  img.className = "img-circle";
	  img.src = "img/human.jpg" ;
	  var src = document.get
	  
	  //var newRow   = tableRef.insertRow(1); //DESC
	  var newRow   = tableRef.insertRow(varItemNo);
	  var newCell  = newRow.insertCell(0);
	  var newCell2  = newRow.insertCell(1);
	  var newCell3  = newRow.insertCell(2);
	  var newCell4  = newRow.insertCell(3);
	  var newCell5  = newRow.insertCell(4);
	  
	  var newText  = document.createTextNode(rowBookImage);
	  var newText2  = document.createTextNode(mem_id);
	  var newText3  = document.createTextNode(uidMsg);
	  var newText4  = document.createTextNode(mem_name);
	  //var newText5  = document.createTextNode(rowBookStatus + "<input type='checkbox' name='myCheckboxes[]' id='myCheckboxes[]' value='someValue2'>");
	  
	  var newTH = document.createElement('th');
	  newTH.innerHTML = "<div class='checkbox'><label style='font-size: 2em'><input type='checkbox' name='myCheckboxes[]' id='myCheckboxes' value='" + mem_id + "' checked><span class='cr'><i class='cr-icon fa fa-check'></i></span></lable></div>";
		
	  newCell.appendChild(img);
	  newCell2.appendChild(newText2);
	  newCell3.appendChild(newText3);
	  newCell4.appendChild(newText4);
	  newCell5.appendChild(newTH);
	  //top.document.getElementById('itemListBody').innerHTML += " Test&nbsp;&nbsp;&nbsp;&nbsp;<br>";
	   
	  //var img = document.createElement("img");
	  document.getElementById('numAddMember').innerHTML = varItemNo;
	  varItemNo++;
	  
	  top.addItemList({'memIdData':mem_id,'uidData':uidMsg,'nameData':mem_name});
	  top.clearTextInput();
	  
	  
	}
    
	function addRowReturnListExcel(tableID,dataMemID,dataUID,dataMemName,rowMemImage) {
		
	  var tableRef = document.getElementById(tableID);
	  var mem_id = dataMemID;
	  var uidMsg = dataUID;
	  var mem_name = dataMemName;
		  
	  var img = document.createElement("img");
	  img.width = "70";
	  img.height = "70";
	  img.className = "img-circle";
	  img.src = "img/human.jpg" ;
	  //img.src = memPicPathNo;
	  var src = document.get
	  
	  //var newRow   = tableRef.insertRow(1); //DESC
	  var newRow   = tableRef.insertRow(varItemNo);
	  var newCell  = newRow.insertCell(0);
	  var newCell2  = newRow.insertCell(1);
	  var newCell3  = newRow.insertCell(2);
	  var newCell4  = newRow.insertCell(3);
	  var newCell5  = newRow.insertCell(4);
	  
	  var newText  = document.createTextNode(rowMemImage);
	  var newText2  = document.createTextNode(mem_id);
	  var newText3  = document.createTextNode(uidMsg);
	  var newText4  = document.createTextNode(mem_name);
	  
          

	  
	  var newTH = document.createElement('th');
	  newTH.innerHTML = "<div class='checkbox'><label style='font-size: 2em'><input type='checkbox' name='myCheckboxes[]' id='myCheckboxes' value='" + mem_id + "' checked><span class='cr'><i class='cr-icon fa fa-check'></i></span></lable></div>";
		
	  newCell.appendChild(img);
	  newCell2.appendChild(newText2);
	  newCell3.appendChild(newText3);
	  newCell4.appendChild(newText4);
	  newCell5.appendChild(newTH);
	  //top.document.getElementById('itemListBody').innerHTML += " Test&nbsp;&nbsp;&nbsp;&nbsp;<br>";
	   
	  //var img = document.createElement("img");
	  document.getElementById('numAddMember').innerHTML = varItemNo;
	  varItemNo++;
	  
	  top.addItemList({'memIdData':mem_id,'uidData':uidMsg,'nameData':mem_name});
	  top.clearTextInput();
	  
	  
	}
</script>
	<style type="text/css">
	.bgSize{
		background-size: 50px 50px;
		background-repeat: no-repeat;
		background-position: center;
	}
	</style> 
</head>
<body>
<div class="container-fluid">
<div class="table-container">
   <div class="table-container-header">
      <table class="table table-striped table-hover table-condensed table-bordered">
         <thead>
            <tr>
                <th style="width: 120px; font-size: 13px; color: dodgerblue; text-align: center;">รูป</th>
                <th style="width: 120px; font-size: 13px; color: dodgerblue; text-align: center;">UID</th>
                <th style="width: 120px; font-size: 13px; color: dodgerblue; text-align: center;">Member ID</th>
                <th style="width: 120px; font-size: 13px; color: dodgerblue; text-align: center;">Member Name</th>
                <th style="width: 120px; font-size: 13px; color: dodgerblue; text-align: center;">&nbsp;<input type='checkbox' id='checkAll'>&nbsp;Select</th>
            </tr>
         </thead>
      </table>
   </div>
   <div class="table-container-body">
   <form id="myform" class="myform" method="post" name="myform">
      <table id="tableItemReturnList" class="table table-striped table-hover table-condensed table-bordered">
         <colgroup>
             <col style="width: 120px;"></col>
             <col style="width: 120px;"></col>
             <col style="width: 120px;"></col>
	     <col style="width: 160px;"></col>
	     <col style="width: 120px;"></col>
         </colgroup>
         <tbody>
            <tr>
            </tr>
         </tbody>
      </table>
		<!--<input id="submit" type="submit" name="submit" value="Submit" onclick="return submitForm()" />-->
		</form>
		 <div id="myResponse"></div>
		 
   </div>
   <div class="table-container-footer">
      <table class="table table-striped table-hover table-condensed table-bordered">
         <tfoot>
            <tr>
               <th style="width: 290px; text-align:right;">Total</th>
               <th style="width: 100px;" class="text-success">
					<label id="numAddMember">0</label><label>&nbsp; Record</label>
			   </th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>	
					<div class="col-sm-1 col-lg-1">&nbsp;</div>
					<div class="col-sm-5 col-lg-5">
                                            <a href="#" onclick="top.iframeURL()"><img src="img/search.png" style="width:10%"></a>
                                            <!--<a href="head_member.php?page=member_search"><img src="img/search.png" style="width:15%"></a>-->
                                                
					</div>
					<div class="col-sm-5 col-lg-5" align="right">
                                            <!--<button type="button" class="btn btn-default" id="checkAll">55</button>-->
					<a href="#" onclick="top.addConfirmModal()"><img src="img/correct.png" style="width:10%;"></a>
				        <a href="#" onclick="reloadPage()"><img src="img/incorrect.png" style="width:10%;"></a>
					</div>
					<div class="col-sm-1 col-lg-1">&nbsp;</div>
		

		
</div>
<script>
 function deleteTable() {
                var dr=0;
                if(confirm("It will be deleted..!!")) {
                    document.getElementById("tableItemReturnList").deleteRow(dr);
                }
            }
			
	function deleteTableAllRecord(){
		//$("#tableItemReturnList").empty();
		$( "#tableItemReturnList tbody tr" ).each( function(){
		this.parentNode.removeChild( this ); 
		});
	}
	
	function deleteRow(el) {
	  var row = upTo(el, 'tbody')
	  if (row) row.parentNode.removeChild(row);
	}
	function deleteRowLoop(){
	var x = document.getElementById("tableItemReturnList").rows.length;
	}
	function remove_rows(el)
	{
	 var x = document.getElementById("tableItemReturnList").rows.length;
	 for(var i = x-1; i >= 0; i--)
	 {
		document.getElementById("tableItemReturnList").deleteRow(i);
	 }
	 var tableRef = document.getElementById(tableItemReturnList);
	}
	
	function reloadPage(){
		top.document.getElementById('itemListHideBody').innerHTML = '';
		top.clearMemberItems();
		location.reload();
	}

	function checkboxSelect(){
		var myCheckboxes = new Array();
		$("input:checked").each(function() {
		   data['myCheckboxes[]'].push($(this).val());
		});
	}
	
	function submitForm() {
		var form = document.myform;

		var dataString = $(form).serialize();


		$.ajax({
			type:'POST',
			url:'test_checkbox_post.php',
			data: dataString,
			success: function(data){
				$('#myResponse').html(data);
			}
		});
		return false;
		}
		
		function showReturnItemListDetail(MEMBER_ITEM_LIST)
		{
				top.document.getElementById('itemListBody').innerHTML = '';
				top.document.getElementById('itemListHideBody').innerHTML = '';
				
				for(var i=0;i<MEMBER_ITEM_LIST.length;i++)
				{
					top.document.getElementById('itemListBody').innerHTML += '<div class="row">';
					top.document.getElementById('itemListBody').innerHTML += '<div class="col-sm-4" ><img src="img/human.jpg" class="img-circle" style="width:70px;height:70px;" ></div>';
					top.document.getElementById('itemListBody').innerHTML += '<div class="col-sm-7" >Name : '+  MEMBER_ITEM_LIST[i]['nameData'] +' mem ID : '+  MEMBER_ITEM_LIST[i]['memIdData'] +' Card ID : '+  MEMBER_ITEM_LIST[i]['uidData'] + '</div>';
					top.document.getElementById('itemListBody').innerHTML += '</div><div class="row">&nbsp;</div><div class="row">&nbsp;</div>';
					//top.document.getElementById('itemListHideBody').innerHTML += '999<input type="input" id="mem_idYut" name="mem_idYut[]" value="Love you"><input type="input" name="mem_id9[]" value="'+  MEMBER_ITEM_LIST[i]['memIdData'] +'"><input type="input" name="uidMsg9[]" value="'+  MEMBER_ITEM_LIST[i]['uidData'] +'"><input type="input" name="mem_name9[]" value="'+  MEMBER_ITEM_LIST[i]['nameData'] +'">';
					
					top.createTextInput(MEMBER_ITEM_LIST[i]['memIdData'],MEMBER_ITEM_LIST[i]['uidData'],MEMBER_ITEM_LIST[i]['nameData']);
					//top.document.getElementById('itemListBody').innerHTML += (i+1) + ". รหัสหนังสือ  :  " + MEMBER_ITEM_LIST[i]['barcode'] + " <br>&nbsp;&nbsp;&nbsp;&nbsp;ชื่อเรื่อง  : " + MEMBER_ITEM_LIST[i]['title'] + " <br>&nbsp;&nbsp;&nbsp;&nbsp;";
					//top.document.getElementById('itemListBody').innerHTML += "กำหนดส่ง : " + MEMBER_ITEM_LIST[i]['end'];
					//top.document.getElementById('itemListBody').innerHTML += "<br><br>";
				}
		}
		
		$(document).ready(function(){
		
		
	
	$( document ).delegate( "#myCheckboxes", "click", function() {
               
		 if(this.checked){
			var selectedIndex = $('input:checkbox').index( $(this) );
                        //alert(selectedIndex);
                        var MyRows = $('table#tableItemReturnList').find('tbody').find('tr');
                        var tbl_mem_id = $(MyRows[selectedIndex+1]).find('td:eq(1)').html();
                        var tbl_uidMsg = $(MyRows[selectedIndex+1]).find('td:eq(2)').html();
                        var tbl_mem_name = $(MyRows[selectedIndex+1]).find('td:eq(3)').html();
                        //alert(tbl_mem_name);
			top.addItemList({'memIdData':tbl_mem_id,'uidData':tbl_uidMsg,'nameData':tbl_mem_name});
		}else{
			var selectedIndex = $('input:checkbox').index( $(this) );
                        var selectedIndex = $('input:checkbox').index( $(this) );
                        //alert(selectedIndex);
                        var MyRows = $('table#tableItemReturnList').find('tbody').find('tr');
                        var tbl_mem_id = $(MyRows[selectedIndex+1]).find('td:eq(1)').html();
                        var tbl_uidMsg = $(MyRows[selectedIndex+1]).find('td:eq(2)').html();
                        var tbl_mem_name = $(MyRows[selectedIndex+1]).find('td:eq(3)').html();
                        //alert(tbl_mem_name);
			//top.removeMemberItems(selectedIndex);
                        top.removeArrayItem(tbl_mem_id);
                        //alert(99999);
                        //alert(selectedIndex);
		}
	 });
         
        $('#checkAll').click(function(){
                //alert($('#checkAll').val());
                
                if ($(this).val() == "check all") 
                { 
                   
                    $(this).data('checked', false);
                   
                    var selectedIndex = $('input:checkbox').index( $(this) );
                        //alert(selectedIndex);
                    var rowCount = $('table#tableItemReturnList').find('tbody').find('tr').length;
                    var MyRows = $('table#tableItemReturnList').find('tbody').find('tr');
                    for($i=1; $i<rowCount; $i++){
                        var tbl_mem_id = $(MyRows[$i]).find('td:eq(1)').html();
                        var tbl_uidMsg = $(MyRows[$i]).find('td:eq(2)').html();
                        var tbl_mem_name = $(MyRows[$i]).find('td:eq(3)').html();
                        //alert(tbl_mem_name);
                        top.addItemList({'memIdData':tbl_mem_id,'uidData':tbl_uidMsg,'nameData':tbl_mem_name});
                        
                    }
                    top
                   $(this).text("uncheck all"); 
                   
                } 
                else 
                { 
                   $(this).data('checked', true);
                   top.clearMemberItems();
                   document.getElementById('numAddMember').innerHTML = "0";
                   $(this).text("check all"); 
                   
                }
                var checked = !$('#checkAll').data('checked');
                $('input:checkbox').prop('checked', checked);
                $(this).val(checked ? 'uncheck all' : 'check all' );
                //$(this).data('checked', checked);
         });
         
         
         
	});
</script>
</body>

</html>