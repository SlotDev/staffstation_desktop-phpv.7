<html>
<head>
<title>TracoN Portal for Control Regor</title>
<link href="favicon.ico" rel="TraceoN icon" type="image/x-icon" />
</head>
<body>
<?PHP

	include "../configuration.php";
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

set_time_limit(-1);

ob_implicit_flush();

function dummyErrorHandler ($errno, $errstr, $errfile, $errline)
{
} 

function forceFlush()
{    
	echo str_pad('',4096)."\n";
    ob_start();
    ob_end_clean();
    flush();
    set_error_handler("dummyErrorHandler");
    ob_end_flush();
    restore_error_handler();
}

$control = "load";
if(isset($_GET["command"]))
{
	$control = $_GET["command"];
}
$loc = "0";
if(isset($_GET["timer"]))
{
	$loc = $_GET["timer"];
}

$host = "127.0.0.1";
$port = 888;
$timeout = -1; //Second.
$regorComd = "REGORFRMWEBCTRL:COM5:";

$function = "readmulti:3"; //Regor Stand by LED Blue for Ready State

switch($control)
{
	case "readmulti":
		$command = $regorComd;
		$function = "readmulti:".$loc; //3 second
		break;
	case "readstart":
		$command = $regorComd;
		$function = "readstart:".$loc;
		break;
	case "readstop":
		$command = $regorComd;
		$function = "readstop:".$loc;
		break;
	case "write":
		$command = $regorComd;
		$function = "write:".$loc;//id 2 tag
		break;
	case "reset":
		$command = $regorComd;
		$function = "reset:".$loc;//RGB
		break;
	default:
}

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}
//echo "Attempting to connect to '$address' on port '$service_port'...";
$result = socket_connect($socket, $host, $port);

//socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec'=>$timeout, 'usec'=>$usec));
//socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, array('sec'=>$timeout, 'usec'=>$usec));

if ($result === false) {
    echo "ERR: socket_connect Failed. $result " . socket_strerror(socket_last_error($socket)) . " END\n";
    echo "<script>parent.tag_write('ENDError');</script>";
}
else
{
	$in = $command.$function;
	$out = '';

	//echo "Sending request...";
	socket_write($socket, $in, strlen($in));
	do
	{
	//echo "Reading response:\n\n";
	$out = socket_read($socket, 4096);
	$out = str_replace("ERR:","END",$out);

	echo $out;
	if($out != ''){
		echo "<script>parent.tag_write('".$out."');</script>";
	}

	forceFlush();
	}while(strpos($out,"END")===false);
}
//echo "Closing socket...";
socket_close($socket);

if(strlen($out)<0)
{
	echo "ERROR END";
}
?>
</body>
</html>