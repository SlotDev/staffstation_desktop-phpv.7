
<?php 
  if(!isset($_SESSION)){
      session_start();
  }
  include "configuration.php";
  include "connect_db.php";
  $lang = $_SESSION['lang'];
    if(isset($_GET['lang'])){
    	$_SESSION['lang'] = $_GET['lang']; //เก็บค่าของภาษาไว้ใน SESSION
      	if($_SESSION['lang'] == "eng"){
        	include "lang_eng.php";
      	}
      	else{
        	include "lang_th.php";
      	}
    }
    else if ($_SESSION['lang'] == 'eng') {
      	include "lang_eng.php";
    }
    else{
      	include "lang_th.php";
    }   
?>
<html lang="en">
 <head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css" >
<link href="jquery-form-validator/validator.css" rel="stylesheet">
<link href="bootstrap-3.3.5-dist/css/TableFixedHeader.css" rel="stylesheet">

<script src="bootstrap-3.3.5-dist/js/jquery.min.js" ></script>
<script src="jquery-form-validator/jquery.form.validator-th.min.js"></script>
<script src="jquery-form-validator/security.js"></script>
<script src="jquery-form-validator/file.js"></script>
<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js" ></script>
<script type="text/javascript">
	member_data = function(name,id,uid,finger,borrowed,booked,overdue,fine,amount,memimg){
            iframe_member.document.location.href = "member_searchIframe2.php?name="+name+"&id="+id+"&uid="+uid+"&finger="+finger+"&borrowed="+borrowed+"&booked="+booked+"&overdue="+overdue+"&fine="+fine+"&amount="+amount+"&$member_image="+memimg;
            document.getElementById('idMemEdit').innerHTML = "<b>รหัสสมาชิก : </b>" + id;
            document.getElementById('mem_id').innerHTML =  id;
            $("#mem_id").val(id);
            document.getElementById('idMemUID').innerHTML = "<b>รหัสบัตร : </b>" + uid;
            document.getElementById('idMemName').innerHTML = "<b>ชื่อ : </b>" + name;
            document.getElementById('idMemDelete').innerHTML = "<b>รหัสสมาชิก : </b>" + id;
            document.getElementById('idMemDeleteUID').innerHTML = "<b>รหัสบัตร : </b>" + uid;
            document.getElementById('idMemDeleteName').innerHTML = "<b>ชื่อ : </b>" + name;	
	}

    function iframeURL()
	{
        window.top.location.href = "http://localhost/TraceoN/staff_station_member/head_member.php?page=member_search#";
    }
	</script>
<style type="text/css">
	.bgSize{
		background-size: 60px 100%;
		background-repeat: no-repeat;
		background-position: center;
	}
</style>
</head>

 <body>
 <div class="container-fluid">
	<div class="panel panel-primary" style="height:11%;background:#2e7ed0;" >
      <div class="panel-body">
        <div class="row">
        	<div class="col-sm-4 col-lg-4" align="left">
        		<img src="img/logo.png" width="50px">
        	</div>
        	<div class="col-sm-4 col-lg-4" align="center">
        		<p class="text-uppercase">
	              <?php if(strlen($lang_orga) < 15){ ?>
	                <font color="#ffffff" size="6"><?php echo $lang_orga; ?></font>
	              <?php }else{ ?>
	                <font color="#ffffff" size="5"><?php echo $lang_orga; ?></font>
	              <?php } ?>
	            </p>
	    	</div>
	    	<div class="col-sm-4 col-lg-4" align="right">
	    		<a href="head.php?page=main_menu"><img src="img/home.png" width="30px" onmouseover="this.src='img/home_1.png'"onmouseout="this.src='img/home.png'"></a>
	    		&nbsp;
	    		<a href="?page=member_search&lang=th"><img src="img/flag_th.png" width="30px"></a>
				<a href="?page=member_search&lang=eng"><img src="img/flag_en.png" width="30px"></a>
        		<p><font color="#ffffff" size="2"><strong><?php echo $name; ?>&nbsp;  </strong></font><a href="logout.php"><strong><u><font color="#ffffff" size="1">  ออกจากระบบ</font></u></strong></a></p>
	    	</div>
	    </div>
	  </div>
	</div>
	<div class="panel panel-primary" style="height:83%;">
		<div class="panel-body">
		<?php 
			switch ($_GET["page"]) {
			case "member_search":
                            include("member_search.php");
                            break;
			case "member_add":
                            include("member_add.php");
                            break;
                        }
		?>
		</div>
	</div>

	<!-- Modal fine-->
	<div class="modal fade" id="fine" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
	      </div>
	      <div class="modal-body">
	       
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal topup-->
	<div class="modal fade" id="topup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">เติมเงิน</h4>
	      </div>
	      <div class="modal-body">
	        <div class="well">
	        	<div class="row">
	        		<div class="col-sm-4 col-lg-4" align="right">
	        			<img src="img/human.jpg" class="img-circle" style="width:80px;height:80px;" >
	        		</div>
	        		<div class="col-sm-8 col-lg-8">
	        			<p id="idMemEdit9"></p>
		  				<p id="idMemName9"></p>
		  				<p id="idMemUID9"></p>
	        		</div>
	        	</div>
	        </div>
	        <div class="panel panel-primary">
	  			<div class="panel-body">
		        	<form class="form-horizontal">
					  <div class="form-group">
					    <label class="col-sm-4 control-label">เงินคงเหลือ</label>
					    <div class="col-sm-6">
					      <input type="text" class="form-control" value="150" disabled="true"> 
					    </div>
					    <label class="col-sm-1 control-label">บาท</label>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-4 control-label">เติมเงิน</label>
					    <div class="col-sm-6">
					      <input type="text" class="form-control" value="100" >
					    </div>
					    <label class="col-sm-1 control-label">บาท</label>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-4 control-label">รวม</label>
					    <div class="col-sm-6">
					      <input type="text" class="form-control" value="250" disabled="true"> 
					    </div>
					    <label class="col-sm-1 control-label">บาท</label>
					  </div>
					</form>
				</div>
	        </div>

	      </div>
	      <div class="modal-footer">
	        <a href="#"><img src="img/correct.png" style="width:50px;height:50px">
	        <a href="#"><img src="img/incorrect.png" style="width:50px;height:50px">
	      </div>
	    </div>
	  </div>
	</div>
 </div>
 <script>
	 $.validate({
		 modules: 'security, file',
		 onModulesLoaded: function () {
		 	$('input[name="pass_confirmation"]').displayPasswordStrength();
		 }
	 });
 </script>

 </body>

 </html>
