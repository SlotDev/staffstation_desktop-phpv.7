<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>	
<?php
	$scoreRule = '-5';
	if(strlen($scoreRule)>0){
		echo $scoreRule = -1 * abs(intval($scoreRule));
	}
?>
<body>

<div id="divTime"></div>

<script type="text/javascript">

function pad2(number) {   
	return (number < 10 ? '0' : '') + number;
}

var timimg = setInterval(function()
{
var date = new Date();
document.getElementById("divTime").innerHTML = date.getDay() + " " + pad2(date.getDate()) + "/" + pad2(date.getMonth()+1) + "/" + date.getFullYear() + " " + pad2(date.getHours()) + ":" + pad2(date.getMinutes()) + ":" + pad2(date.getSeconds());
},1000);
</script>

</body>
</html>